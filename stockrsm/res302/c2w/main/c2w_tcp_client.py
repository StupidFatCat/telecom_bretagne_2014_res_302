#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import subprocess
import os

parser = argparse.ArgumentParser(description='c2w client (TCP Version)')
parser.add_argument('-e', '--debug',
                    dest='debugFlag',
                    help='Raise the log level to debug',
                    action="store_true",
                    default=False)

options = parser.parse_args()
cmdLine = [os.path.join(os.path.dirname(__file__), 'c2w_client.py')]
cmdLine.append('-P c2w.protocol.tcp_chat_client.c2wTcpChatClientProtocol')

if options.debugFlag:
    cmdLine.append('-e')

print 'about to call: ', cmdLine
try:
    retcode = subprocess.call(cmdLine)
except KeyboardInterrupt:
    pass  # ignore CTRL-C
