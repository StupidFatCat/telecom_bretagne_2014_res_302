#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import subprocess
import os

parser = argparse.ArgumentParser(description='c2w client (UDP Version)')
parser.add_argument('-e', '--debug',
                    dest='debugFlag',
                    help='Raise the log level to debug',
                    action="store_true",
                    default=False)
parser.add_argument('-l', '--loss-pr', dest='lossPr',
                    help='The packet loss probability for outgoing ' +
                    'packets.', type=float, default=0)

options = parser.parse_args()
cmdLine = [os.path.join(os.path.dirname(__file__), 'c2w_client.py')]
cmdLine.append('--udp')
cmdLine.append('-P c2w.protocol.udp_chat_client.c2wUdpChatClientProtocol')

if options.debugFlag:
    cmdLine.append('-e')
if options.lossPr > 0:
    cmdLine.append('-l ' + str(options.lossPr))

print 'about to call: ', cmdLine
try:
    retcode = subprocess.call(cmdLine)
except KeyboardInterrupt:
    pass  # ignore CTRL-C
