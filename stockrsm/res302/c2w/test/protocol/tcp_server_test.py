from twisted.trial import unittest
from twisted.test import proto_helpers
from twisted.internet import task

from c2w.protocol.tcp_chat_server import c2wTcpChatServerProtocol
from c2w.main.server_model import c2wServerModel
from c2w.main.server_proxy import c2wServerProxy
import yaml
import re
import os
import sys
from collections import namedtuple
import logging
from twisted.internet import reactor

logC2w = logging.getLogger('c2w')
logC2w.setLevel(logging.DEBUG)

MOVIES = [
          (0X01, "3 Days to Kill", "0.0.0.0", 20000, ""),
          (0X02, "Braddock America", "0.0.0.0", 20001, "")
          ]


def get_id(s):
    return int(s.split(':')[0].lstrip('#'))


def get_edge(s):
    return s.split(':')[1]


class c2wTcpChatServerTestCase(unittest.TestCase):

    def _get_automata_path(self):
        return "g10"

    def init_clients(self, clients):

        for c in clients:
            transport = proto_helpers.StringTransport()
            protocol = c2wTcpChatServerProtocol(self.serverProxy,
                                                "127.0.0.1",
                                                9999 + c)
            self.protocols[c] = (transport, protocol)
            protocol.makeConnection(transport)

    def setUp(self):

        self.automata_dir = os.path.dirname(__file__)
        self.client_fsm = None
        self.automata = None

        self.serverModel = c2wServerModel()
        self.serverProxy = c2wServerProxy(self.serverModel)
        self.serverProxy.initMovieStore(False, True)

        self.serverProxy.removeAllMovies()
        for m in MOVIES:
            self.serverProxy.addMovie(m[1], m[2], m[3], m[4])

        self.split = False
        self.protocols = {}


    def test_one_user_login_tcp_server_test_1by1(self):
        self.automata = os.path.join(self._get_automata_path(),
                                     'one_user_login_tcp_server_test_1by1.yaml')
        self.split = True
        self.test_automata()
        
    def test_one_user_login_userlist_movielist_tcp_server_test_1by1(self):
        self.automata = os.path.join(self._get_automata_path(),
                                     'one_user_login_userlist_movielist_tcp_server_test_1by1.yaml')
        self.split = True
        self.test_automata()
    def test_two_users_login_movielist_tcp_server_test_1by1(self):
        self.automata = os.path.join(self._get_automata_path(),
                                     'two_users_login_movielist_tcp_server_test_1by1.yaml')
        self.split = True
        self.test_automata()


    def test_automata(self):

        self.client_fsm = yaml.load(open(os.path.join(self.automata_dir,
                                                      self.automata)))

        clients = set()
        for v in self.client_fsm.values():
            action, edges = v
            if edges:
                for e in edges.items():
                    client_id = get_id(e[0])
                    clients.add(client_id)

        self.init_clients(clients)
        automata_state = namedtuple('automata_state', 'action edges')

        state = self.client_fsm['Init']
        label = 'Init'
        state = automata_state._make(state)  # now we can write t.action and t.edges
        while True:
            assert(type(state.action) is str)
            if state.action.startswith("F"):
                forward_time = float(re.search(r"(F )([0-9]*\.?[0-9]*)",
                                               state.action).
                                     group(2))
                self.clock.advance(forward_time)

            assert(type(state.edges) is dict)

            success = False
            client_id = None
            bad_client_id = True
            for numbered_edge, next_state in state.edges.items():
                edge_id, edge = get_id(numbered_edge), get_edge(numbered_edge)
                condition, action = edge.split('/')
                if condition:
                    v = self.protocols[edge_id][0].value()
                    hex_v = "".join("{0:#0{1}x}".format(ord(i), 4)[2:] for i in v)
                    if hex_v:
                        bad_client_id = False
                        client_id = edge_id
                        if hex_v == condition:
                            print "Test passed"
                            self.assertEqual(hex_v, condition)
                            self.protocols[edge_id][0].clear()
                            success = True
                        else:
                            failed_condition = condition
                            failed_hex_v = hex_v
                    else:
                        pass  # ??
                if ((action and not condition) or (action and success)):
                    s = ""
                    for i in xrange(0, len(action), 2):
                        s = s + chr(int(action[i: i + 2], 16))
                    if self.split:
                        for s_b in s:
                            self.protocols[edge_id][1].dataReceived(s_b)
                    else:
                        self.protocols[edge_id][1].dataReceived(s)
                    state = automata_state._make(self.client_fsm[next_state])
                    label = next_state
                    break
                if not action and success:
                    state = automata_state._make(self.client_fsm[next_state])
                    label = next_state
                    break
            else:
                if label == "Final":
                    print "Rock'n Roll !"
                    pending = reactor.getDelayedCalls()
                    for p in pending:
                        if p.active():
                            p.cancel()
                    return

                if client_id is not None:
                    if not bad_client_id:
                        pending = reactor.getDelayedCalls()
                        for p in pending:
                            if p.active():
                                p.cancel()
                        self.assertEqual(failed_hex_v, failed_condition)
                    else:
                        self.assertFalse(bad_client_id, "Bad client identification")
                else:
                    pending = reactor.getDelayedCalls()
                    for p in pending:
                        if p.active():
                            p.cancel()
                    self.fail("Damned, I'm stymied !")
