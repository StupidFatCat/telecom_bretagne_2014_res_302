#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import os
import logging
import importlib
from twisted.internet import reactor
from sibyl_brain import SibylBrain
from sibyl_tcp_server_factory import SibylTcpSeverProtocolFactory


def main():
    """The main for the twisted Sibyl client"""
    #logging.basicConfig()
    log = logging.getLogger('sibyl_client')
    log.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(levelname)s] [%(name)s] %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    log.addHandler(handler)

    parser = argparse.ArgumentParser(description='Sibyl Client')
    parser.add_argument('-p', '--port', dest='server_port', type=int,
                        default=1800,
                        help='The port number to be used for listening.')
    parser.add_argument('-P', '--protocol',
                        dest='protocol',
                        help='The name of the protocol class to be used',
                        action='store',
                        default=os.environ.get('SYBIL_PROTOCOL', None))
    parser.add_argument('-e', '--debug',
                        dest='debugFlag',
                        help='Raise the log level to debug',
                        action="store_true",
                        default=False)
    parser.add_argument('-1', '--udp-text', dest='udp_text',
                        help='Use the UDP version of the text protocol' +
                        '(SibylServerUdpTextProtocol class)',
                        action="store_true")
    parser.add_argument('-2', '--udp-bin', dest='udp_bin',
                        help='Use the UDP version of the binary protocol' +
                        '(SibylServerUdpBinProtocol class)',
                        action="store_true")
    parser.add_argument('-3', '--tcp-text', dest='tcp_text',
                        help='Use the TCP version of the text protocol' +
                        '(SibylSeverTcpTextProtocol class)',
                        action="store_true")
    parser.add_argument('-4', '--tcp-bin', dest='tcp_bin',
                        help='Use the TCP version of the binary protocol' +
                        '(SibylServerTcpBinProtocol class)',
                        action="store_true")
    parser.add_argument('-5', '--udp-timer-bin', dest='udp_bin_timer',
                        help='Use the UDP version of the binary protocol' +
                        'with the time (SibylServerTimerUdpBinProtocol class)',
                        action="store_true")
    options = parser.parse_args()

    if options.server_port > 65536 or options.server_port <= 1500:
        print "Invalid port: the port number has to be between 1501 and 65536"
        exit(-1)

    if options.debugFlag:
        log.setLevel(logging.DEBUG)

    sibylBrain = SibylBrain()

    usingUdp = False
    if options.udp_text:
        options.protocol = 'sibyl.protocol.sibyl_server_udp_text'
        options.protocol += '_protocol.SibylServerUdpTextProtocol'
        usingUdp = True
        log.info('using the UDP text protocol')
    elif options.udp_bin:
        options.protocol = 'sibyl.protocol.sibyl_server_udp_bin'
        options.protocol += '_protocol.SibylServerUdpBinProtocol'
        usingUdp = True
        log.info('using the UDP binary protocol')
    elif options.udp_bin_timer:
        options.protocol = 'sibyl.protocol.sibyl_server_timer_udp_bin'
        options.protocol += '_protocol.SibylServerTimerUdpBinProtocol'
        usingUdp = True
        log.info('using the UDP binary protocol (timer version)')
    elif options.tcp_text:
        options.protocol = 'sibyl.protocol.sibyl_server_tcp_text'
        options.protocol += '_protocol.SibylServerTcpTextProtocol'
        usingUdp = False
        log.info('using the TCP text protocol')
    elif options.tcp_bin:
        options.protocol = 'sibyl.protocol.sibyl_server_tcp_bin'
        options.protocol += '_protocol.SibylServerTcpBinProtocol'
        usingUdp = False
        log.info('using the TCP binary protocol')

    log.debug('module name=%s', str(options.protocol.rsplit('.', 1)[0]))
    log.debug('protocol name=%s', str(options.protocol.rsplit('.', 1)[1]))
    module = importlib.import_module(options.protocol.rsplit('.', 1)[0])
    protocolName = getattr(module, options.protocol.rsplit('.', 1)[1])
    log.debug("importing module=%s, protocol=%s", str(module),
              str(protocolName))

    if usingUdp:
        serverProtocolInstance = protocolName(sibylBrain)
        log.debug('protocol instance created')
        reactor.listenUDP(options.server_port, serverProtocolInstance)
        log.info('Server listening (UDP) on port %d', options.server_port)
    else:
        reactor.listenTCP(options.server_port,
                          SibylTcpSeverProtocolFactory(protocolName,
                                                       sibylBrain))
        log.info('Server listening (TCP) on port %d', options.server_port)
    log.info("MAIN_INFO: Starting the reactor")
    reactor.run()

if __name__ == '__main__':
    main()
