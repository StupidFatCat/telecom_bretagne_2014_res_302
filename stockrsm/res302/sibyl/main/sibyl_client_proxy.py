# -*- coding: utf-8 -*-
import sys
import logging

try:
    from twisted.internet.endpoints import TCP4ClientEndpoint
    from twisted.internet import reactor
except ImportError:
    print "IMPORT_ERROR: Unable to import twisted module"
    sys.exit(1)

# try:
#     from c2w_main.c2w_chat_client_protocol_factory \
#         import c2wChatClientProtocolFactory
# except ImportError:
#     print "IMPORT_ERROR: Unable to import the c2wChatClientProtocolFactory module"
#     sys.exit(1)

moduleLogger = logging.getLogger('sibyl_client.sibyl_client_proxy')


class SibylClientProxy(object):
    """
    This class is the interface between the SibylClientProtocol and
    the SibylController (which is the controller of the client).

    .. warning::
        Your protocol implementation can interact with the user interface
        only by calling the methods of this class.

    .. note::
        You cannot force the user interface to show (or hide) a certain window.
        The controller takes care of this whenever appropriate.
        For example, it hides the initial window whenever your protocol
        calls the :py:meth:`~c2w_main.c2w_client_proxy.c2wClientProxy.\
initCompleteONE`.  Similarly it shows the appropriate window whenever your
        protocol calls :py:meth:`~c2w_main.c2w_client_proxy.c2wClientProxy.\
joinRoomOKONE`.
    """
    def __init__(self, sibylController):
        """
        :param sibylController: The corresponding SibylController instance.
        """
        moduleLogger.debug("SibylClientProxy constructor started")
        self.sibylController = sibylController
        self.protocolInstance = None

    def registerProtcolInstance(self, protocolInstance):
        self.protocolInstance = protocolInstance

    def sendRequest(self, requestText):
        """
        :param requestText: The text of the request (question for the Sibyl).

        Called **by the sibylController** when the user clicks on
        the "send reqeust" button. This function instantiates the connection
        end-point, with the corresponding ChatClientProtocolFactory.
        """
        moduleLogger.debug("sendRequest started")
        self.protocolInstance.sendRequest(requestText)

    def responseReceived(self, responseText):
        self.sibylController.responseReceived(responseText)

    def connectionSuccess(self):
        """
        Called by the TCP version of the protocol when the connection
        with the server is established.  (Needed by the GUI.)
        """
        self.sibylController.connectionSuccess()
