# -*- coding: utf-8 -*-
import random


class SibylBrain(object):
    def __init__(self, randomly=True):
        self.randomly = randomly
        self.responses = ["All work and no play makes Jack a dull boy",
                    "I've got a feeling we're not in Kansas anymore",
                    "People who live in glass houses should not throw stones",
                    "Two wrongs don't make a right"]

    def generateResponse(self, questionText):
        if self.randomly:
            return random.choice(self.responses)
        else:
            return self.responses[0]
