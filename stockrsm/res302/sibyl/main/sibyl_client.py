#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import os
import logging
import sys
import importlib

try:
    from twisted.internet import gtk3reactor  # for gtk-3.0
    try:
        gtk3reactor.install()
    except Exception, e:
        print "[*] ERROR: Could not initiate GTK modules: %s" % (e)
        sys.exit(1)
    from twisted.internet import reactor
except ImportError:
    print "[*] ERROR: Could not import Twisted Network Framework"
    sys.exit(1)

from sibyl_client_app import SibylClientApplication
from sibyl_controller import SibylTextInterfaceController


def main():
    """The main for the twisted Sibyl client"""
    #logging.basicConfig()
    log = logging.getLogger('sibyl_client')
    log.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(levelname)s] [%(name)s] %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    log.addHandler(handler)

    parser = argparse.ArgumentParser(description='Sibyl Client')
    parser.add_argument('-p', '--port', dest='port', type=int, default=1800,
                    help='the server port number to connect to')
    parser.add_argument("-m", "--machine", dest="host", type=str,
                        default='localhost',
                        help="the server name or IP address to connect to")
    parser.add_argument('-P', '--protocol',
                        dest='protocol',
                        help='The name of the protocol class to be used',
                        action='store',
                        default=os.environ.get('SYBIL_PROTOCOL', None))
    parser.add_argument('-e', '--debug',
                        dest='debugFlag',
                        help='Raise the log level to debug',
                        action="store_true",
                        default=False)
    parser.add_argument('-1', '--udp-text', dest='udp_text',
                        help='Use the UDP version of the text protocol' +
                        '(SibylClientUdpTextProtocol class)',
                        action="store_true")
    parser.add_argument('-2', '--udp-bin', dest='udp_bin',
                        help='Use the UDP version of the binary protocol' +
                        '(SibylClientUdpBinProtocol class)',
                        action="store_true")
    parser.add_argument('-3', '--tcp-text', dest='tcp_text',
                        help='Use the TCP version of the text protocol' +
                        '(SibylClientTcpTextProtocol class)',
                        action="store_true")
    parser.add_argument('-4', '--tcp-bin', dest='tcp_bin',
                        help='Use the TCP version of the binary protocol' +
                        '(SibylClientTcpBinProtocol class)',
                        action="store_true")
    parser.add_argument('-t', '--text-user-interface',
                        dest='text_user_interface',
                        help='Use the text version of the user interface.',
                        action="store_true")
    options = parser.parse_args()

    if options.port > 65536 or options.port <= 1500:
        print "Invalid port: the port number has to be between 1501 and 65536"
        exit(-1)

    if options.debugFlag:
        log.setLevel(logging.DEBUG)

    usingUdp = False
    if options.udp_text:
        options.protocol = 'sibyl.protocol.sibyl_client_udp_text'
        options.protocol += '_protocol.SibylClientUdpTextProtocol'
        usingUdp = True
    elif options.udp_bin:
        options.protocol = 'sibyl.protocol.sibyl_client_udp_bin'
        options.protocol += '_protocol.SibylClientUdpBinProtocol'
        usingUdp = True
    elif options.tcp_text:
        options.protocol = 'sibyl.protocol.sibyl_client_tcp_text'
        options.protocol += '_protocol.SibylClientTcpTextProtocol'
    elif options.tcp_bin:
        options.protocol = 'sibyl.protocol.sibyl_client_tcp_bin'
        options.protocol += '_protocol.SibylClientTcpBinProtocol'

    module = importlib.import_module(options.protocol.rsplit('.', 1)[0])
    protocolName = getattr(module, options.protocol.rsplit('.', 1)[1])
    log.debug("importing module=%s, protocol=%s", module, protocolName)

    if options.text_user_interface:
        log.info('Starting the text-based user interface')
        SibylTextInterfaceController(protocolName,
                                usingUdp, options.host, options.port)
    else:
        log.info("MAIN_INFO: Starting the SibylClientApp")
        sibylClientAppInstance = SibylClientApplication(protocolName,
                                usingUdp, options.host, options.port)
        log.info("MAIN_INFO: Registering the sibylClientApp")
        reactor.registerGApplication(sibylClientAppInstance)

    log.info("MAIN_INFO: Starting the reactor")
    reactor.run()


if __name__ == '__main__':
    main()
