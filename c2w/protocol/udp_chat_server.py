# -*- coding: utf-8 -*-
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from c2w.main.lossy_transport import LossyTransport
import logging
import ctypes
import struct
from c2w.main.constants import ROOM_IDS
logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.udp_chat_server_protocol')


class c2wUdpChatServerProtocol(DatagramProtocol):

    def __init__(self, serverProxy, lossPr):
        """
        :param serverProxy: The serverProxy, which the protocol must use
            to interact with the user and movie store (i.e., the list of users
            and movies) in the server.
        :param lossPr: The packet loss probability for outgoing packets.  Do
            not modify this value!

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: serverProxy

            The serverProxy, which the protocol must use
            to interact with the user and movie store in the server.

        .. attribute:: lossPr

            The packet loss probability for outgoing packets.  Do
            not modify this value!  (It is used by startProtocol.)

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """

        self.serverProxy = serverProxy
        self.lossPr = lossPr
        # dictionnaire qui associe à chaque adresse : le num de seq attendue de
        # ce client(sera initialisé à 1 parce que l'utilisateur aura deja
        # envoyé un msg pour se connecter) , le dernier num de seq envoyée par
        # le serveur à ce client(mis à 0 parceque le premier msg que le serveur
        # enverra aura le num 0), l'état du client (0 initial ,1 ack
        # login_resp_ok reçu, 2: ack userlist reçu, 3:ack movielist reçu, 4
        # sinon) l'identifiant numérique de l'utilisateur (Id) , le reactor
        # objet qui permet de mettre un timer , le dernier buffer envoyé à
        # l'utilisateur, et si le serveur attend un acquittement .
        self.users = {}
    
    def startProtocol(self):
        """
        DO NOT MODIFY THE FIRST TWO LINES OF THIS METHOD!!

        If in doubt, do not add anything to this method.  Just ignore it.
        It is used to randomly drop outgoing packets if the -l
        command line option is used.
        """
        self.transport = LossyTransport(self.transport, self.lossPr)
        DatagramProtocol.transport = self.transport

    def datagramReceived(self, datagram, (host, port)):
        """
        :param string datagram: the payload of the UDP packet.
        :param host: the IP address of the source.
        :param port: the source port.

        Called **by Twisted** when the server has received a UDP
        packet.
        """
        print "(host,port)", (host, port)
        header = self.unpack_header(datagram)
        typep = header[0:4]
        ackflag = header[4]
        reserved = header[5]
        ack_num = int(header[6:19], 2)
        seq_num = int(header[19:32], 2)
        msg_len = self.unpack_msg_len(datagram)
        msg_len = int("0b" + msg_len, 2)
        cleexists = False
        print "nv paquet recu"
        print "typep", typep
        for cle in self.users:
            # savoir si l'adresse a déja été enregistrée dans le dictionnaire
            if cle == (host, port):
                cleexists = True

       # Si l'acquittement de la reponse de login req a été perdu
        if typep == '0001' and cleexists == True:
            if self.users[(host, port)][2] == 1:
                buff = self.users[(host, port)][5]
                self.send_packet(buff, (host, port))
                print "error"

        if typep!="1111" and typep!="0001":
            if self.users[(host,port)][0]-1==seq_num:
                Id=self.users[(host,port)][3]
                self.acknowledgement(seq_num,Id)
                print "error"

        # msg reçu de type login request (champ data = username)
        if typep == "0001":
            # client a déja envoyé une demande de connexion avec erreur
            if cleexists == True and self.users[(host, port)][2] == 0:
                # if seq_num==0:
                print "login request received"
                username = struct.unpack_from(
                    '>IH' + str(msg_len - 6) + 's', datagram, 0)[2]
                self.login_response_msg(username, host, port)
            elif cleexists == False:
                print "login request received"
                username = struct.unpack_from(
                    '>IH' + str(msg_len - 6) + 's', datagram, 0)[2]
                self.login_response_msg(username, host, port)
        # print "self.users +++",self.users[(host,port)][0]
        print "seq_num+++", seq_num

        if self.users[(host, port)][0] == seq_num + 1 and seq_num != 0:
            self.acknowledgement(seq_num, self.users[(host, port)][3])

        if self.users[(host, port)][0] == seq_num:
            # msg reçu de type disconnect (champ data = Id)
            if typep == "1000":
                Id = struct.unpack_from('>IHH', datagram, 0)[2]
                sq_waited = self.users[(host, port)][0]
                username = self.serverProxy.getUserById(Id).userName
                if seq_num == sq_waited:
                    print"leave request received"
                    self.acknowledgement(seq_num, Id)
                    print"leave ack sent"
                    self.serverProxy.removeUser(username)
                    users = self.serverProxy.getUserList()
                    for user in users:
                        self.user_list(user.userId, user.userAddress)

                    del self.users[(host, port)]

            if typep == "0110":  # msg reçu de type MovieRoom Request
                print "join movie room request received"
                Id = struct.unpack_from('>IHHHHH', datagram, 0)[2]
                ip1 = str(int(self.convert_int_to_bin(
                    struct.unpack_from('>IHHHHH', datagram, 0)[3], 16)[0:8], 2))
                ip2 = str(int(self.convert_int_to_bin(
                    struct.unpack_from('>IHHHHH', datagram, 0)[3], 16)[8:16], 2))
                ip3 = str(int(self.convert_int_to_bin(
                    struct.unpack_from('>IHHHHH', datagram, 0)[4], 16)[0:8], 2))
                ip4 = str(int(self.convert_int_to_bin(
                    struct.unpack_from('>IHHHHH', datagram, 0)[4], 16)[8:16], 2))
                movieIp = ip1 + '.' + ip2 + '.' + ip3 + '.' + ip4
                moviePort = struct.unpack_from('>IHHHHH', datagram, 0)[5]
                username = self.serverProxy.getUserById(Id).userName
                sq_waited = self.users[(host, port)][0]
                if seq_num == sq_waited:
                    for movie in self.serverProxy.getMovieList():
                        if movie.moviePort == moviePort and movie.movieIpAddress == movieIp:
                            title = movie.movieTitle
                    self.serverProxy.updateUserChatroom(username, title)
                    self.serverProxy.startStreamingMovie(title)
                    self.acknowledgement(seq_num, Id)
                    print "ack join movie room sent"
                    self.users[(host, port)][0] += 1 % 8192
                    users = self.serverProxy.getUserList()
                    for user in users:
                        if user.userChatRoom == ROOM_IDS.MAIN_ROOM and user.userId != Id:
                            self.user_list(user.userId, user.userAddress)
                            print 'user.userId', user.userId
                        elif user.userChatRoom == title:
                            self.user_list_movieroom(
                                user.userId, user.userAddress, title)
                            print "user.userId22", user.userId

            # msg reçu de type MainRoom request (champ data = Id)
            if typep == "0111":
                print "join main room request received"
                Id = struct.unpack_from('>IHH', datagram, 0)[2]
                sq_waited = self.users[(host, port)][0]
                username = self.serverProxy.getUserById(Id).userName
                room = self.serverProxy.getUserById(Id).userChatRoom
                self.serverProxy.stopStreamingMovie(room)
                self.serverProxy.updateUserChatroom(
                    username, ROOM_IDS.MAIN_ROOM)
                if seq_num == sq_waited:
                    print self.serverProxy.getUserById(Id).userChatRoom
                    self.acknowledgement(seq_num, Id)
                    print "ack join main room sent"
                    self.users[(host, port)][0] += 1 % 8192
                    users = self.serverProxy.getUserList()
                    for user in users:
                        if user.userChatRoom == ROOM_IDS.MAIN_ROOM:
                            self.user_list(user.userId, user.userAddress)
                        elif user.userChatRoom == room and user.userId != Id:
                            self.user_list_movieroom(
                                user.userId, user.userAddress, room)

            if typep == "0100":  # msg reçu de type message de chat
                Id = struct.unpack_from(">IHHHHH", datagram, 0)[2]
                ip1 = self.convert_int_to_bin(
                    struct.unpack_from('>IHHHHH', datagram, 0)[3], 16)
                ip2 = self.convert_int_to_bin(
                    struct.unpack_from('>IHHHHH', datagram, 0)[4], 16)
                moviePort = self.convert_int_to_bin(
                    struct.unpack_from('>IHHHHH', datagram, 0)[5], 16)
                movie_id = ip1 + ip2 + moviePort
                message = struct.unpack_from(
                    ">IHHHHH" + str(msg_len - 14) + 's', datagram, 0)[6]
                sq_waited = self.users[(host, port)][0]
                if seq_num == sq_waited:
                    self.users[(host, port)][0] += 1 % 8192
                    self.acknowledgement(seq_num, Id)
                    print "ack msg chat sent"
                    room = self.serverProxy.getUserById(Id).userChatRoom
                    users = self.serverProxy.getUserList()
                    for user in users:
                        if user.userChatRoom == room and user.userId != Id:
                            self.sendMsg(
                                user.userId, Id, message, room, movie_id, user.userAddress)

        if ackflag == '1':
            print "++++", ack_num
            print "++++", self.users[(host, port)][1]
        if ackflag == '1' and ack_num == self.users[(host, port)][1]:
            if typep == "1111":  # msg reçu de type acquittement
                self.users[(host, port)][6]=0
                address = (host, port)
                if self.serverProxy.getUserByAddress(address) != None:
                    Id = self.serverProxy.getUserByAddress(address).userId
                print "etat", self.users[(host, port)][2]
                print "ack_num", ack_num
                print "dernier de seq envoyé +1 ", self.users[(host, port)][1]
                if self.users[(host, port)][2] == 1:  # ack login_resp_ok reçu
                    if self.users[(host, port)][4] != '':
                        self.users[(host, port)][4].cancel()
                        print "cancel"
                        self.users[(host, port)][4] = ''
                    self.users[(host, port)][1] += 1
                    self.user_list(Id, (host, port))
                    print "self.users[(host,port)][1]", self.users[(host, port)][1]
                    self.users[(host, port)][2] = 2

                elif self.users[(host, port)][2] == 2:  # ack userlist reçu
                    if self.users[(host, port)][4] != '':
                        self.users[(host, port)][4].cancel()
                        print "cancel"
                        self.users[(host, port)][4] = ''
                    self.users[(host, port)][1] += 1
                    self.movie_list(Id, host, port)
                    self.users[(host, port)][2] = 3

                elif self.users[(host, port)][2] == 3:  # ack movielist reçu
                    if self.users[(host, port)][4] != '':
                        self.users[(host, port)][4].cancel()
                        print "cancel"
                        self.users[(host, port)][4] = ''
                    self.users[(host, port)][1] += 1
                    room = self.serverProxy.getUserById(Id).userChatRoom
                    users = self.serverProxy.getUserList()
                    username = self.serverProxy.getUserById(Id).userName
                    self.serverProxy.updateUserChatroom(
                        username, ROOM_IDS.MAIN_ROOM)
                    for user in users:
                        if user.userName != self.serverProxy.getUserById(Id).userName and user.userChatRoom == ROOM_IDS.MAIN_ROOM:
                            self.user_list(user.userId, user.userAddress)

                    self.users[(host, port)][2] = 4
                else:
                    if self.users[(host, port)][4] != '':
                        self.users[(host, port)][4].cancel()
                        
                        print "cancel"
                        self.users[(host, port)][4] = ''
                        self.users[(host, port)][1] += 1

    def acknowledgement(self, ack_num, Id):
        self.type = "1111"
        self.ack_flag = "1"
        self.reserved = "0"
        self.ack_num = ack_num
        self.seq_num = 0
        self.data_len = 0
        self.header = self.create_header(
            self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.msg_len = self.create_msg_len(self.data_len)
        self.buffer = self.prepare_buffer_ack(self.header, self.msg_len)
        user = self.serverProxy.getUserById(Id)
        useradress = user.userAddress
        self.send_packet_ack(self.buffer, useradress)
        print "ack envoyé n° ", ack_num

    # fonction qui envoie soit un msg de type login response ou Error Message
    def login_response_msg(self, username, host, port):
        self.users[(host, port)] = [1, 0, 0, 0, '', '',0]
        if self.serverProxy.userExists(username) == True:
            address = (host, port)
            self.error_msg(1, address)
            self.users[(host, port)][2] = 0
        elif len(username) > 128:
            address = (host, port)
            self.error_msg(1, address)
            self.users[(host, port)][2] = 0
        else:
            Id = self.serverProxy.addUser(username, ROOM_IDS.MAIN_ROOM, None, (host, port))
            self.users[(host, port)][3] = Id
            # envoi d'un acquittement contenant l'Id
            self.login_response_ok(Id, host, port)
            self.users[(host, port)][2] = 1

    # fonction qui crée un message login response
    def login_response_ok(self, Id, host, port):
        self.type = "1110"
        self.ack_flag = "1"
        self.reserved = "0"
        self.ack_num = self.users[(host, port)][0] - 1
        self.seq_num = self.users[(host, port)][1]
        print"NUM=", self.users[(host, port)][1]
        self.data_len = 2
        self.header = self.create_header(
            self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.msg_len = self.create_msg_len(self.data_len)
        self.buffer = self.prepare_buffer_userid(
            self.header, self.msg_len, Id, self.data_len)
        user = self.serverProxy.getUserById(Id)
        useradress = user.userAddress
        self.send_packet(self.buffer, useradress)
        print "Id sent"
        users = self.serverProxy.getUserList()

    # fonction qui crée un message de type error Error message
    def error_msg(self, code, address):
        self.type = "0000"
        self.ack_flag = "1"
        self.reserved = "0"
        self.ack_num = self.users[address][0] - 1
        print"NUM=", self.users[address][1]
        self.seq_num = self.users[address][1]
        self.data_len = 2
        self.header = self.create_header(
            self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.msg_len = self.create_msg_len(self.data_len)
        self.buffer = self.prepare_buffer_error(
            self.header, self.msg_len, code)
        self.send_packet(self.buffer, address)

    def user_list(self, Id, (host, port)):
        self.type = "0010"
        self.ack_flag = "0"
        self.reserved = "0"
        self.ack_num = 0
        print"NUM=", self.users[(host, port)][1]
        self.seq_num = self.users[(host, port)][1]
        header = self.create_header(
            self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.data_len = 2
        for user in self.serverProxy.getUserList():
            self.data_len += 3 + len(user.userName)
        msg_len = self.create_msg_len(self.data_len)
        self.buffer = self.prepare_buffer_userList(header, msg_len)
        user = self.serverProxy.getUserById(Id)
        useradress = user.userAddress
        self.send_packet(self.buffer, useradress)
        print "user list sent"

    def user_list_movieroom(self, Id, (host, port), title):
        self.type = "0010"
        self.ack_flag = "0"
        self.reserved = "0"
        self.ack_num = 0
        print"NUM=", self.users[(host, port)][1]
        self.seq_num = self.users[(host, port)][1]
        header = self.create_header(
            self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.data_len = 2
        number = 0
        for user in self.serverProxy.getUserList():
            if user.userChatRoom == title:
                self.data_len += 3 + len(user.userName)
                number += 1
        msg_len = self.create_msg_len(self.data_len)
        self.buffer = self.prepare_buffer_userListm(
            header, msg_len, number, title)
        user = self.serverProxy.getUserById(Id)
        useradress = user.userAddress
        self.send_packet(self.buffer, useradress)
        print "user list update sent"

    def movie_list(self, Id, host, port):
        self.type = "0011"
        self.ack_flag = "0"
        self.reserved = "0"
        self.ack_num = 0
        self.seq_num = self.users[(host, port)][1]
        print"NUM=", self.users[(host, port)][1]
        header = self.create_header(
            self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.data_len = 2
        for movie in self.serverProxy.getMovieList():
            self.data_len += 7 + len(movie.movieTitle)
        msg_len = self.create_msg_len(self.data_len)
        self.buffer = self.prepare_buffer_movieList(header, msg_len)
        user = self.serverProxy.getUserById(Id)
        useradress = user.userAddress
        self.send_packet(self.buffer, useradress)
        print "movie list sent"

    def sendMsg(self, Id, user_id, message, room, adresse, (host, port)):
        self.type = "0100"
        self.ack_flag = "0"
        self.reserved = "0"
        self.ack_num = 0
        self.seq_num = self.users[(host, port)][1]
        print"NUM=", self.users[(host, port)][1]

        if room == ROOM_IDS.MAIN_ROOM:
            movie_adr = "0" * 48
            self.data_len = 8 + len(message)
            self.header = self.create_header(
                self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
            self.msg_len = self.create_msg_len(self.data_len)

        else:
            movie_adr = adresse
            self.data_len = 8 + len(message)
            self.header = self.create_header(
                self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
            self.msg_len = self.create_msg_len(self.data_len)

        self.buffer = self.prepare_buffer_msg(
            self.header, self.msg_len, user_id, self.data_len, movie_adr, message)
        self.send_packet(self.buffer, (host, port))

    # fonction qui permet d'avoir une chaine de car de 32 bits contenant le
    # header
    def unpack_header(self, datagram):
        header = struct.unpack_from('>IH', datagram)[0]
        header = bin(header)
        header = str(header)[2:]
        z = "0" * (32 - len(header))
        header = z + header
        return header

    # fonction qui permet d'avoir une chaine de car de 16 bits contenant le
    # msg_len
    def unpack_msg_len(self, datagram):
        msg_len = struct.unpack_from('>IH', datagram)[1]
        msg_len = bin(msg_len)
        msg_len = str(msg_len)[2:]
        z = "0" * (16 - len(msg_len))
        msg_len = z + msg_len
        return msg_len

    def create_header(self, typep, ackflag, reserved, ack_num, seq_num):
        ack_num = str(bin(ack_num)[2:])
        z = str("0" * (13 - len(ack_num)))
        ack_num = z + ack_num
        seq_num = str(bin(seq_num)[2:])
        z = str("0" * (13 - len(seq_num)))
        seq_num = z + seq_num
        header = int(typep + ackflag + reserved + ack_num + seq_num, 2)
        return header

    def create_msg_len(self, data_len):
        msg_len = data_len + 6
        msg_len = str(bin(msg_len)[2:])
        z = str("0" * (16 - len(msg_len)))
        msg_len = z + msg_len
        msg_len = int(msg_len, 2)
        return msg_len

    def prepare_buffer(self, header, msg_len, data, data_len):
        buff = ctypes.create_string_buffer(6 + data_len)
        struct.pack_into(
            '>IH' + str(data_len) + 's', buff, 0, header, msg_len, data)
        return buff

    def prepare_buffer_error(self, header, msg_len, data):
        buff = ctypes.create_string_buffer(8)
        struct.pack_into('>IHH', buff, 0, header, msg_len, data)
        return buff

    def prepare_buffer_userid(self, header, msg_len, data, data_len):
        buff = ctypes.create_string_buffer(6 + data_len)
        struct.pack_into('>IHH', buff, 0, header, msg_len, data)
        return buff

    def prepare_buffer_ack(self, header, msg_len):
        buff = ctypes.create_string_buffer(6)
        struct.pack_into('>IH', buff, 0, header, msg_len)
        return buff

    def prepare_buffer_userList(self, header, msg_len):
        users = self.serverProxy.getUserList()
        # constition de head_list : champ Roo + Number
        roo = "00"
        number = len(users)
        number = self.convert_int_to_bin(number, 14)
        head_list = int(roo + number, 2)

        # constitution de la liste

        userList = ''
        self.buff = ctypes.create_string_buffer(msg_len)
        struct.pack_into('>IHH', self.buff, 0, header, msg_len, head_list)
        self.offset = 8
        for user in users:

            # constitution du champ length
            len_username = len(user.userName)
            data_len = 3 + len_username

            len_username = self.convert_int_to_bin(len_username, 7)
            user_id = user.userId

            user_id = self.convert_int_to_bin(user_id, 16)
            # constitution du statut
            room = user.userChatRoom
            if room == ROOM_IDS.MAIN_ROOM:
                s = "1"
            else:
                s = "0"
            struct.pack_into('>BH' + str(len(user.userName)) + 's', self.buff,
                             self.offset, int((len_username) + s, 2), int(user_id, 2), user.userName)
            self.offset += data_len
        return self.buff

    def prepare_buffer_userListm(self, header, msg_len, number, title):
        users = self.serverProxy.getUserList()
        # constition de head_list : champ Roo + Number
        roo = "00"
        number = self.convert_int_to_bin(number, 14)
        head_list = int(roo + number, 2)

        # constitution de la liste

        userList = ''
        self.buff = ctypes.create_string_buffer(msg_len)
        struct.pack_into('>IHH', self.buff, 0, header, msg_len, head_list)
        self.offset = 8
        for user in users:
            if user.userChatRoom == title:
                # constitution du champ length
                len_username = len(user.userName)
                data_len = 3 + len_username

                len_username = self.convert_int_to_bin(len_username, 7)
                user_id = user.userId

                user_id = self.convert_int_to_bin(user_id, 16)
                # constitution du statut
                room = user.userChatRoom
                if room == ROOM_IDS.MAIN_ROOM:
                    s = "1"
                else:
                    s = "0"
                struct.pack_into('>BH' + str(len(user.userName)) + 's', self.buff,
                                 self.offset, int((len_username) + s, 2), int(user_id, 2), user.userName)
                self.offset += data_len
        return self.buff

    def prepare_buffer_movieList(self, header, msg_len):
        movies = self.serverProxy.getMovieList()
        # constition de head_list
        head_list = len(movies)

        # constitution de la liste

        self.buff = ctypes.create_string_buffer(msg_len)
        struct.pack_into('>IHH', self.buff, 0, header, msg_len, head_list)
        self.offset = 8
        for movie in movies:

            # constitution du champ length
            len_title = len(movie.movieTitle)
            data_len = 7 + len_title

            len_title = self.convert_int_to_bin(len_title, 8)
            movieip = movie.movieIpAddress
            movieIP = movieip.split('.', 3)
            for i in range(0, 4):
                movieIP[i] = self.convert_int_to_bin(int(movieIP[i]), 8)
            moviePort = movie.moviePort
            moviePort = int(self.convert_int_to_bin(moviePort, 16), 2)
            movieTitle = movie.movieTitle
            struct.pack_into('>HHHB' + str(len(movie.movieTitle)) + 's', self.buff, self.offset, int(movieIP[
                             0] + movieIP[1], 2), int(movieIP[2] + movieIP[3], 2), moviePort, int(len_title, 2), movie.movieTitle)
            self.offset += data_len
        return self.buff

    # fonction qui permet de convertir un entier en une chaine de caractere de
    # la representation binaire de cet entier avec une taille predefinie .
    def convert_int_to_bin(self, integ, taille):
        sortie = str(bin(integ)[2:])
        z = str("0" * (taille - len(sortie)))
        sortie = z + sortie
        return sortie

    def prepare_buffer_msg(self, header, msg_len, user_id, data_len, movie_adr, message):
        buff = ctypes.create_string_buffer(6 + data_len)
        struct.pack_into('>IHHHHH' + str(len(message)) + 's', buff, 0, header, msg_len, user_id,
                         int(movie_adr[0:16], 2), int(movie_adr[16:32], 2), int(movie_adr[32:48], 2), message)
        return buff

    def send_packet(self, buff, clientAddress, i=0):
        #print "i", i
        
        
        i += 1
        if self.users[clientAddress][6]==1 and self.users[clientAddress][4]=='':
            reactor.callLater(0.01, self.send_packet, buff, clientAddress)
            print "wait"
            
        else:
            self.users[clientAddress][5] = buff
            self.transport.write(buff.raw, clientAddress)
            self.users[clientAddress][4] = reactor.callLater(
                2, self.send_packet, buff, clientAddress, i)
            self.users[clientAddress][6]=1
        
            

    def send_packet_ack(self, buff, clientAddress):
        self.transport.write(buff.raw, clientAddress)
