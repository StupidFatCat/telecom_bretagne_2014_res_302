import ctypes
import struct


def create_header(typep, ackflag, reserved, ack_num, seq_num):
    ack_num = bin(ack_num)[2:]
    z = str("0" * (13 - len(ack_num)))
    ack_num = z + ack_num
    seq_num = bin(seq_num)[2:]
    z = str("0" * (13 - len(seq_num)))
    seq_num = z + seq_num
    header = int(typep + ackflag + reserved + ack_num + seq_num, 2)
    return header


def create_msg_len(data_len):
    msg_len = data_len + 6
    msg_len = bin(msg_len)[2:]
    z = str("0" * (16 - len(msg_len)))
    msg_len = z + msg_len
    msg_len = int(msg_len, 2)
    return msg_len


# fonction qui permet d'avoir une chaine de car de 32 bits contenant le header
def unpack_header(datagram):
    header = struct.unpack_from('>IH', datagram)[0]
    header = bin(header)[2:]
    z = "0" * (32 - len(header))
    header = z + header
    return header


# fonction qui permet d'avoir une chaine de car de 16 bits contenant le msg_len
def unpack_msg_len(datagram):
    msg_len = struct.unpack_from('>IH', datagram)[1]
    msg_len = bin(msg_len)[2:]
    z = "0" * (16 - len(msg_len))
    msg_len = z + msg_len
    return msg_len


# fonction qui permet de convertir un entier en une chaine de caractere de
# la representation binaire de cet entier avec une taille predefinie .
def convert_int_to_bin(integ, taille):
    sortie = bin(integ)[2:]
    z = str("0" * (taille - len(sortie)))
    sortie = z + sortie
    return sortie
