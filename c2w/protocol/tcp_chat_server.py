# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol
import logging
from utile import *
from c2w.main.constants import ROOM_IDS
from twisted.internet import reactor
logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.tcp_chat_server_protocol')


class c2wTcpChatServerProtocol(Protocol):

    def __init__(self, serverProxy, clientAddress, clientPort):
        """
        :param serverProxy: The serverProxy, which the protocol must use
            to interact with the user and movie store (i.e., the list of users
            and movies) in the server.
        :param clientAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param clientPort: The port number used by the c2w server,
            given by the user.

        Class implementing the TCP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: serverProxy

            The serverProxy, which the protocol must use
            to interact with the user and movie store in the server.

        .. attribute:: clientAddress

            The IP address (or the name) of the c2w server.

        .. attribute:: clientPort

            The port number used by the c2w server.

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.

        .. note::
            The IP address and port number of the client are provided
            only for the sake of completeness, you do not need to use
            them, as a TCP connection is already associated with only
            one client.
        """
        self.clientAddress = clientAddress
        self.clientPort = clientPort
        self.serverProxy = serverProxy

        #############################################################
        # self.seq_client = 0  
        self.ack_cmp = 0  # compteur du numéro d'acquittement
        self.seq_cmp = 0  # compteur du numéro de séquence
        self.etat_client = 0  # etat de client
        # 0 initialemen
        # 1 login sans erreur
        # 2 login aquittee.
        # 3 user_list aquittee.
        # 4 movie_list acquitee
        self.send_wait = 0
        self.user_id = 0
        self.buff = ''
        self.taille = 99999999

    def dataReceived(self, data):
        """
        :param data: The message received from the server
        :type data: A string of indeterminate length

        Twisted calls this method whenever new data is received on this
        connection.
        """
        self.buff = self.buff + data
        print "--->data received"
        while(len(self.buff) >= 6):
            print 'len_buff:', len(self.buff)

            if(self.taille == 99999999):
                self.taille = struct.unpack_from('>IH', self.buff)[1]
                print 'taille:', self.taille
            if len(self.buff) >= self.taille:
                header = unpack_header(self.buff)
                typep = header[0:4]
                ackflag = header[4]
                reserved = header[5]
                ack_num = int(header[6:19], 2)
                seq_num = int(header[19:32], 2)
                print 'receive_seq:', seq_num
                msg_len = unpack_msg_len(self.buff)
                msg_len = int(msg_len, 2)
                print 'msg_len:', msg_len

                ##############################################
                # if seq_num != self.seq_cmp:
                # envoie msg error

                # msg recu de type login request (data = username)
                if typep == "0001":
                    # if seq_num == 0:
                    print '--->recois un login message'
                    username = struct.unpack_from(
                        '>IH' + str(msg_len - 6) + 's', self.buff, 0)[2]
                    self.login_response_msg(seq_num, username)
                    # uid = self.serverProxy.getUserByName(username).userId

                # msg recu de type acquittement
                if typep == "1111":
                    print '!!!user_etat:', self.etat_client
                    # print '!!!user_seq:', self.seq_client
                    print '!!!user_ack_num:', ack_num
                    print 'ackflag:', ackflag
                    print 'serveur seq cmp:', self.seq_cmp
                    self.send_wait = 0
                    if ackflag == '1' and ack_num == self.seq_cmp - 1:
                        # login sans erreur
                        if self.etat_client == 1:
                            self.etat_client += 1
                            self.user_list(ROOM_IDS.MAIN_ROOM)

                        # user_list acquitee
                        elif self.etat_client == 2:
                            self.etat_client += 1
                            self.movie_list()

                        # movie_list acquitee
                        # apres on fait mise a jour le user_list pour les autres users.
                        elif self.etat_client == 3:
                            self.etat_client += 1
                            username = self.serverProxy.getUserById(
                                self.user_id).userName
                            self.serverProxy.updateUserChatroom(
                                username, ROOM_IDS.MAIN_ROOM)
                            room = self.serverProxy.getUserById(
                                self.user_id).userChatRoom
                            users = self.serverProxy.getUserList()
                            for user in users:
                                if user.userChatRoom == room and user.userId != self.user_id:
                                    user.userChatInstance.user_list(
                                        ROOM_IDS.MAIN_ROOM)
                                    print 'sent userlist for:', user.userName
                        

                # msg recu de type publique message de chat
                if typep == "0100":
                    ip1 = convert_int_to_bin(
                        struct.unpack_from('>IHHHHH', self.buff, 0)[3], 16)
                    ip2 = convert_int_to_bin(
                        struct.unpack_from('>IHHHHH', self.buff, 0)[4], 16)
                    moviePort = convert_int_to_bin(
                        struct.unpack_from('>IHHHHH', self.buff, 0)[5], 16)
                    movie_id = ip1 + ip2 + moviePort
                    message = struct.unpack_from(
                        ">IHHHHH" + str(msg_len - 14) + 's', self.buff, 0)[6]

                    self.acknowledgement(seq_num)
                    room = self.serverProxy.getUserById(
                        self.user_id).userChatRoom
                    print "self.user_id is:", self.user_id  # jiangfan
                    users = self.serverProxy.getUserList()
                    print "users are :", users  # jiang fan
                    for user in users:
                        if user.userChatRoom == room and user.userId != self.user_id:
                            user.userChatInstance.sendMsg(
                                self.user_id, message, movie_id)

                # msg recu de type MainRoom request( data = user_id )
                if typep == "0111":
                    print "---> join main room request received"
                    Id = struct.unpack_from('>IHH', self.buff, 0)[2]
                    user1 = self.serverProxy.getUserById(Id)
                    room = user1.userChatRoom
                    if user1.userChatRoom != ROOM_IDS.MAIN_ROOM:
                        self.serverProxy.stopStreamingMovie(user1.userChatRoom)
                        self.serverProxy.updateUserChatroom(
                            user1.userName, ROOM_IDS.MAIN_ROOM)

                        self.acknowledgement(seq_num)

                        users = self.serverProxy.getUserList()
                        for user in users:
                            print 'user et room:', user.userName, user.userChatRoom
                            print 'user1 room:', user1.userChatRoom
                            if user.userChatRoom == ROOM_IDS.MAIN_ROOM:
                                user.userChatInstance.user_list(
                                    ROOM_IDS.MAIN_ROOM)
                                print "sent userlist", user.userName
                            elif user.userChatRoom == room:
                                user.userChatInstance.user_list(room)
                                print "sent userList movie :", user.userName

                # msg recu de type MovieRoom Request
                if typep == "0110":
                    print "---> join movie room request received"
                    Id = struct.unpack_from('>IHHHHH', self.buff, 0)[2]
                    ip1 = str(int(convert_int_to_bin(
                        struct.unpack_from('>IHHHHH', self.buff, 0)[3], 16)[0:8], 2))
                    ip2 = str(int(convert_int_to_bin(
                        struct.unpack_from('>IHHHHH', self.buff, 0)[3], 16)[8:16], 2))
                    ip3 = str(int(convert_int_to_bin(
                        struct.unpack_from('>IHHHHH', self.buff, 0)[4], 16)[0:8], 2))
                    ip4 = str(int(convert_int_to_bin(
                        struct.unpack_from('>IHHHHH', self.buff, 0)[4], 16)[8:16], 2))
                    movieIp = ip1 + '.' + ip2 + '.' + ip3 + '.' + ip4
                    moviePort = struct.unpack_from('>IHHHHH', self.buff, 0)[5]
                    user1 = self.serverProxy.getUserById(Id)

                    for movie in self.serverProxy.getMovieList():
                        if movie.moviePort == moviePort and movie.movieIpAddress == movieIp:
                            title = movie.movieTitle
                            break
                    self.serverProxy.updateUserChatroom(user1.userName, title)
                    print user1.userName, 'is in movie room:', title

                    self.acknowledgement(seq_num)
                    print "ack join movie room sent"

                    self.serverProxy.startStreamingMovie(title)
                    users = self.serverProxy.getUserList()
                    for user in users:
                        if user.userChatRoom == ROOM_IDS.MAIN_ROOM:
                            user.userChatInstance.user_list(ROOM_IDS.MAIN_ROOM)
                            print "sent userlist", user.userName
                        elif user.userChatRoom == title:
                            user.userChatInstance.user_list(title)
                            # jiangfan
                            print 'users.userChatRoom is :', user.userChatRoom
                            # jiangfan
                            print 'user.userChatInstance:', user.userChatInstance
                            print "sent userList movie :", user.userName

                # msg recu de type disconnect (champ data = user_id)
                if typep == "1000":
                    Id = struct.unpack_from('>IHH', self.buff, 0)[2]
                    user1 = self.serverProxy.getUserById(Id)

                    self.acknowledgement(seq_num)
                    self.serverProxy.removeUser(user1.userName)
                    print "leave ack sent"
                    users = self.serverProxy.getUserList()
                    for user in users:
                        user.userChatInstance.user_list(ROOM_IDS.MAIN_ROOM)

                if self.taille <= 0:
                    self.taille = 1
                self.buff = self.buff[self.taille:]
                self.taille = 99999999
            else:
                break

    def acknowledgement(self, acknum):
        self.prepare_header("1111", "1", 0, acknum)
        self.seq_num = 0
        self.header = create_header(
            self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.msg_len = create_msg_len(self.data_len)

        self.buffer = self.prepare_buffer_ack()
        self.send_packet_ack(self.buffer)
        print "le serveur a envoyé un ack pour le msg du client num", self.ack_num

    def login_response_msg(self, ack_num, username):
        if len(username) > 128:
            self.prepare_header("0000", "1", 1, ack_num)
            self.buffer = self.prepare_buffer_err('\1')
        elif self.serverProxy.userExists(username) == False:
            self.user_id = self.serverProxy.addUser(
                username, ROOM_IDS.MAIN_ROOM, self)  
            self.prepare_header("1110", "1", 2, ack_num)
            self.buffer = self.prepare_buffer_userid()
            self.etat_client = 1
        else:
            self.prepare_header("0000", "1", 1, ack_num)
            self.buffer = self.prepare_buffer_err('\1')

        self.send_packet(self.buffer)
        print "<---envoie un login_response_msg"

    def sendMsg(self, sourceUserId, message, movie_adr):
        self.prepare_header("0100", "0", 8 + len(message))

        self.header = create_header(
            self.type, self.ack_flag, self.reserved, 0, self.seq_num)
        self.msg_len = create_msg_len(self.data_len)

        self.buffer = self.prepare_buffer_msg(sourceUserId, message, movie_adr)
        self.send_packet(self.buffer)

    def prepare_header(self, typep, ackflag, length, acknum = 0):
        self.type = typep
        self.ack_flag = ackflag
        self.reserved = "0"
        self.ack_num = acknum
        self.seq_num = self.seq_cmp
        self.data_len = length

        self.header = create_header(
            self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.msg_len = create_msg_len(self.data_len)

###### ack_num seq_num ?????#################

##### msg_len ? octets ####
    def prepare_buffer_err(self, data):
        buff = ctypes.create_string_buffer(7)
        struct.pack_into('>IH1s', buff, 0, self.header, self.msg_len, data)
        return buff
####

    def prepare_buffer_ack(self):
        buff = ctypes.create_string_buffer(6)
        struct.pack_into('>IH', buff, 0, self.header, self.msg_len)
        return buff

####
    def prepare_buffer_msg(self, sourceUserId, message, movie_adr):
        buff = ctypes.create_string_buffer(6 + self.data_len)
        struct.pack_into('>IHHHHH' + str(len(message)) + 's', buff, 0, self.header, self.msg_len,
                         sourceUserId, int(movie_adr[0:16], 2), int(movie_adr[16:32], 2), int(movie_adr[32:48], 2), message)
        return buff

    def prepare_buffer_userid(self):
        buff = ctypes.create_string_buffer(8)
        struct.pack_into(
            '>IHH', buff, 0, self.header, self.msg_len, self.user_id)
        return buff

    def prepare_buffer_userList(self, title, number):
        users = self.serverProxy.getUserList()
        # constition de head_list : champ Roo + Number
        roo = "00"
        number = convert_int_to_bin(number, 14)
        head_list = int(roo + number, 2)

        # constitution de la liste
        userList = ''
        buff = ctypes.create_string_buffer(6 + self.data_len)
        struct.pack_into('>IHH', buff, 0, self.header, self.msg_len, head_list)
        offset = 8
        for user in users:
            if user.userChatRoom == title or title == ROOM_IDS.MAIN_ROOM:
                # constitution du champ length
                len_username = len(user.userName)
                data_len = 3 + len_username

                len_username = convert_int_to_bin(len_username, 7)
                user_id = user.userId

                user_id = convert_int_to_bin(user_id, 16)
                # constitution du statut
                room = user.userChatRoom
                if room == ROOM_IDS.MAIN_ROOM:
                    s = "1"
                else:
                    s = "0"
                struct.pack_into('>BH' + str(len(user.userName)) + 's', buff,
                                 offset, int((len_username) + s, 2), int(user_id, 2), user.userName)
                offset += data_len
        return buff

    def prepare_buffer_movieList(self):
        movies = self.serverProxy.getMovieList()
        # constition de head_list
        head_list = len(movies)

        # constitution de la liste

        buff = ctypes.create_string_buffer(6 + self.data_len)
        struct.pack_into('>IHH', buff, 0, self.header, self.msg_len, head_list)
        offset = 8
        for movie in movies:

            # constitution du champ length
            len_title = len(movie.movieTitle)
            data_len = 7 + len_title

            len_title = convert_int_to_bin(len_title, 8)
            movieip = movie.movieIpAddress
            movieIP = movieip.split('.', 3)
            for i in range(0, 4):
                movieIP[i] = convert_int_to_bin(int(movieIP[i]), 8)
            moviePort = movie.moviePort
            moviePort = int(convert_int_to_bin(moviePort, 16), 2)
            movieTitle = movie.movieTitle
            struct.pack_into('>HHHB' + str(len(movie.movieTitle)) + 's', buff, offset, int(movieIP[0] + movieIP[
                             1], 2), int(movieIP[2] + movieIP[3], 2), moviePort, int(len_title, 2), movie.movieTitle)
            offset += data_len
        return buff

    def user_list(self, title):
        print "<---envoie un user_list:", title
        print 'serveur seq_num:', self.seq_num
        self.data_len = 2
        number = 0
        for user in self.serverProxy.getUserList():
            if user.userChatRoom == title or title == ROOM_IDS.MAIN_ROOM:
                self.data_len += 3 + len(user.userName)
                number += 1

        self.prepare_header("0010", "0", self.data_len)

        self.header = create_header(
            self.type, self.ack_flag, self.reserved, 0, self.seq_num)
        self.msg_len = create_msg_len(self.data_len)

        self.buffer = self.prepare_buffer_userList(title, number)
        self.send_packet(self.buffer)
        print "user list sent"

    def movie_list(self):
        print "<---envoie un movie_list"
        print 'serveur seq_num:', self.seq_num
        self.data_len = 2
        for movie in self.serverProxy.getMovieList():
            # dans le code udp c'est 8?
            self.data_len += 7 + len(movie.movieTitle)

        self.prepare_header("0011", "0", self.data_len)

        self.header = create_header(
            self.type, self.ack_flag, self.reserved, 0, self.seq_num)
        self.msg_len = create_msg_len(self.data_len)

        self.buffer = self.prepare_buffer_movieList()
        self.send_packet(self.buffer)
        print "movie list sent"

    def send_packet(self, buff):
        print 'sent packet len:', len(buff.raw)
        # self.seq_client += 1 % 8912
        if self.send_wait == 1:
            reactor.callLater(0.01, self.send_packet, buff)
            print 'wait'
        else:
            self.transport.write(buff.raw)
            self.seq_cmp += 1 % 8192
            self.send_wait = 1

    def send_packet_ack(self, buff):
        self.transport.write(buff.raw)
