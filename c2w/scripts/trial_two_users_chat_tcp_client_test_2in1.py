#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import subprocess


def main():
    trial_class = 'c2w.test.protocol.tcp_client_test.c2wTcpChatClientTestCase'
    cmdLine = ['trial',
               trial_class + '.test_two_users_chat_tcp_client_test_2in1']
    try:
        retcode = subprocess.call(cmdLine)
    except KeyboardInterrupt:
        pass  # ignore CTRL-C


if __name__ == '__main__':
    main()
