#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse
import subprocess
import os

def main():
    trial_class = 'c2w.test.protocol.udp_client_test.c2wUdpChatClientTestCase'
    cmdLine = ['trial',
               trial_class + '.test_one_user_login_userlist_movielist_reverse_udp_client_test']
    try:
        retcode = subprocess.call(cmdLine)
    except KeyboardInterrupt:
        pass  # ignore CTRL-C


if __name__ == '__main__':
    main()
