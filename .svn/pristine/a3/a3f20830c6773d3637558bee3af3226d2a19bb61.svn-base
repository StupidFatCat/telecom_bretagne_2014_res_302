# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol
import logging
import ctypes
import struct
from c2w.main.constants import ROOM_IDS
from utile import *

logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.tcp_chat_client_protocol')

class c2wTcpChatClientProtocol(Protocol):

    def __init__(self, clientProxy, serverAddress, serverPort):
        """
        :param clientProxy: The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.
        :param serverAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param serverPort: The port number used by the c2w server,
            given by the user.

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: clientProxy

            The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        .. attribute:: serverAddress

            The IP address (or the name) of the c2w server.

        .. attribute:: serverPort

            The port number used by the c2w server.

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """
        self.serverAddress = serverAddress
        self.serverPort = serverPort
        self.clientProxy = clientProxy


        self.ack_cmp=0 #compteur du numéro d'acquittement
        self.seq_cmp=0 #compteur du numéro de séquence
        self.user_id=0
        self.salleactuelle='' #type de salle où l'utilisateur se trouve en ce moment
        self.movie_id=''
        self.userlist=[]
        self.movielist=[]
        self.etatClient='' #cette variable indique si le client attend un acquittement ou pas.

    def sendLoginRequestOIE(self, userName):
        """
        :param string userName: The user name that the user has typed.

        The client proxy calls this function when the user clicks on
        the login button.
        """
        moduleLogger.debug('loginRequest called with username=%s', userName)

        self.prepare_header("0001", "0", len(username))
        
        if self.etatClient == 'ready':
            self.ack_flag = "1"

        # self.incrementer_seqcompteur()

        self.header = create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) #Fonction qu'on a ajouté voir fin du fichier
        self.msg_len = create_msg_len(self.data_len) #Fonction qu'on a ajouté voir fin du fichier
        self.buffer = self.prepare_buffer_login(self.header,self.msg_len,userName,self.data_len) #Fonction qu'on a ajouté voir fin du fichier

        self.send_packet(self.buffer,self.serverAddress,self.serverPort) #Fonction qu'on a ajouté voir fin du fichier
        self.etatClient='waitLoginAck'

    def sendChatMessageOIE(self, message):
        """
        :param message: The text of the chat message.
        :type message: string

        Called by the client proxy when the user has decided to send
        a chat message

        .. note::
           This is the only function handling chat messages, irrespective
           of the room where the user is.  Therefore it is up to the
           c2wChatClientProctocol or to the server to make sure that this
           message is handled properly, i.e., it is shown only by the
           client(s) who are in the same room.
        """

        self.prepare_header("0100", "0", 8+len(message))
        
        # self.incrementer_seqcompteur()
         
        if self.salleactuelle=='main':
            movie_id="0"*48
            self.header = create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) 
            self.msg_len = create_msg_len(self.data_len) 
            
            
        elif self.salleactuelle[0:5]=='movie':
            self.header = create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) 
            self.msg_len = create_msg_len(self.data_len)
            for movie in self.movielist:
                if movie[0]==self.salleactuelle[6:]:
                    movieIP=movie[1]
                    moviePort=movie[2]
            movieIP=movieIP.split('.',3)
            for i in range(0,4):
                movieIP[i] = convert_int_to_bin(int(movieIP[i]),8)
            moviePort = convert_int_to_bin(moviePort,16)
            movie_id=movieIP[0]+movieIP[1]+movieIP[2]+movieIP[3]+moviePort

        self.buffer=self.prepare_buffer_msg(self.header,self.msg_len,self.user_id,self.data_len,movie_id,message) 
        self.send_packet(self.buffer,self.serverAddress,self.serverPort)
        
        self.etatClient='waitMsgAck'

    def sendJoinRoomRequestOIE(self, roomName):
        """
        :param roomName: The room name (or movie title.)

        Called by the client proxy  when the user
        has clicked on the watch button or the leave button,
        indicating that she/he wants to change room.

        .. warning:
            The controller sets roomName to
            c2w.main.constants.ROOM_IDS.MAIN_ROOM when the user
            wants to go back to the main room.
        """
        if roomName==ROOM_IDS.MAIN_ROOM:

            self.prepare_header("0111", "0", 2)
            # self.incrementer_seqcompteur()
            self.header = create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num)
            self.msg_len = create_msg_len(self.data_len) 

            self.buffer=self.prepare_buffer_joinroom(self.header,self.msg_len,self.user_id,self.data_len) 
            self.send_packet(self.buffer,self.serverAddress,self.serverPort) 
            self.etatClient='waitJoinMainAck'
            self.salleactuelle='main'
            
            
        else:
            self.prepare_header("0110", "0", 8)
            # self.incrementer_seqcompteur()
            self.header = create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num)
            self.msg_len = create_msg_len(self.data_len) 

            for movie in self.movielist:
                if movie[0]==roomName:
                    movieIP=movie[1]
                    moviePort=movie[2]
            self.clientProxy.updateMovieAddressPort(roomName, movieIP, moviePort)
            movieIP=movieIP.split('.',3)
            for i in range(0,4):
                movieIP[i] = convert_int_to_bin(int(movieIP[i]),8)
            moviePort = convert_int_to_bin(moviePort,16)
            movie_id=movieIP[0]+movieIP[1]+movieIP[2]+movieIP[3]+moviePort

            self.buffer=self.prepare_buffer_joinroommov(self.header,self.msg_len,self.user_id,movie_id,self.data_len)
            self.send_packet(self.buffer,self.serverAddress,self.serverPort)
            self.etatClient='waitJoinMovieAck'
            self.salleactuelle='movie:'+roomName

    def sendLeaveSystemRequestOIE(self):
        """
        Called by the client proxy  when the user
        has clicked on the leave button in the main room.
        """

        self.prepare_header("1000", "0", 2)

        self.header = create_header(self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.msg_len = create_msg_len(self.data_len)

        self.buffer = self.prepare_buffer_logout(self.header, self.msg_len, self.user_id, self.data_len)
        self.send_packet(self.buffer, self.serverAddress, self.serverPort)

        self.etatClient = 'waitLeaveAck'


################################################################################
    def prepare_header(self, typep, ackflag, length):
        self.type = typep
        self.ack_flag=ackflag
        self.reserved="0"
        self.ack_num=self.ack_cmp
        self.seq_num=self.seq_cmp
        self.data_len = length

    def prepare_buffer_login(self,header,msg_len,data,data_len):
        buff=ctypes.create_string_buffer(6+data_len)
        struct.pack_into('IH'+str(data_len)+'s',buff,0,header,msg_len,data)
        return buff
       
    def prepare_buffer_joinroom(self,header,msg_len,data,data_len):
        buff=ctypes.create_string_buffer(6+data_len)
        struct.pack_into('IHH',buff,0,header,msg_len,data)
        return buff
    
    def prepare_buffer_joinroommov(self,header,msg_len,userid,movieid,data_len):
        buff=ctypes.create_string_buffer(14)
        struct.pack_into('IHHHHH',buff,0,header,msg_len,userid,int(movieid[0:16],2),int(movieid[16:32],2),int(movieid[32:48],2))
        return buff
    
    def prepare_buffer_msg(header,msg_len,user_id,data_len,movie_id,message):
        buff=ctypes.create_string_buffer(6+data_len)
        struct.pack_into('IHHH'+str(len(message))+'s',buff,0,header,msg_len,user_id,movie_id,message)
        return buff
    
    def prepare_buffer_ack(self,header,msg_len):
        buff=ctypes.create_string_buffer(6)
        struct.pack_into('IH',buff,0,header,msg_len)
        return buff
    
        
    def send_packet(self,buff,serverAddress,serverPort):
        self.transport.write(buff,(serverAddress,serverPort))
    
    def incrementer_seqcompteur(self):
        if self.seq_cmp==8191:
            self.seq_cmp=0
        else:
            self.seq_cmp+=1
            print '+'
            
    def incrementer_ackcompteur(self):
        if self.ack_cmp==8191:
            self.ack_cmp=0
        else:
            self.ack_cmp+=1

    def dataReceived(self, data):
        """
        :param data: The message received from the server
        :type data: A string of indeterminate length

        Twisted calls this method whenever new data is received on this
        connection.
        """
        header=self.unpack_header(datagram)
        typep=header[0:4]
        ackflag=header[4]
        reserved=header[5]
        ack_num=int(header[6:19],2)
        seq_num=int(header[19:32],2)
        msg_len=self.unpack_msg_len(datagram)
        msg_len=int(msg_len,2)
        print "seqnum",self.seq_cmp
        print "acknum",self.ack_cmp
        
        print "type=",typep
        
        #le client reçoit response_login_ok
        if typep == "1110" and self.etatClient=='waitLoginAck': 
            if ackflag == '1' and ack_num == self.seqnum
                self.incrementer_seqcompteur()
                self.user_id=struct.unpack_from('IHH',datagram,0)[2]
                self.etatClient='waitUserList'
                # self.incrementer_ackcompteur()


        
        #le client reçoit msg d'erreur    
        if typep=="0000" and self.etatClient=='waitLoginAck': 
            if ackflag == '1' and ack_num == self.seqnum
                self.incrementer_seqcompteur()
                data=struct.unpack_from('IHH',datagram,0)[2]
                self.etatClient='ready'
                print "error=",data
                if data == 1:
                    self.clientProxy.connectionRejectedONE("Invalid User Name/Connection Denied")
                if data == 2:
                    self.clientProxy.connectionRejectedONE("Unknown User")
                if data == 3:
                    self.clientProxy.connectionRejectedONE("Unknown Movie")
                if data == 4:
                    self.clientProxy.connectionRejectedONE("Malformed Message")
                    # self.incrementer_ackcompteur()

        #le client reçoit la liste des utilisateurs
        if typep == "0010" and self.etatClient=='waitUserList':
            if ackflag == '1' and ack_num == self.seqnum
                self.incrementer_seqcompteur()
                number=struct.unpack_from('IHH',datagram,0)[2]
                self.etatClient='waitMovieList'
                offset=8
                for i in range(0,number):
                    len_username=bin(struct.unpack_from('HH',datagram,offset)[0])
                    s=len_username[len(len_username)-1]
                    len_username=int(len_username[2:len(len_username)-1],2)/256
                    username=struct.unpack_from('HH'+str(len_username)+'s',datagram,offset)[2]
                    offset+=4+len_username
                    if s=='1':
                        self.userlist.append((username,ROOM_IDS.MAIN_ROOM))
                    else:
                        self.userlist.append((username,ROOM_IDS.MOVIE_ROOM))

        if typep == "0011" and self.etatClient=='waitMovieList':#le client reçoit la liste des films
            
            number=struct.unpack_from('IHH',datagram,0)[2]
            offset=8
            for i in range(0,number):
                len_title=bin(struct.unpack_from('HHHH',datagram,offset)[3])
                len_title=int(len_title,2)/256
                title=struct.unpack_from('HHHH'+str(len_title)+'s',datagram,offset)[4]
                movieIP=[]
                movieIP.append(str(int(self.convert_int_to_bin(struct.unpack_from('HHHH',datagram,offset)[0],16)[0:8],2)))
                movieIP.append(str(int(self.convert_int_to_bin(struct.unpack_from('HHHH',datagram,offset)[0],16)[8:16],2)))
                movieIP.append(str(int(self.convert_int_to_bin(struct.unpack_from('HHHH',datagram,offset)[1],16)[0:8],2)))
                movieIP.append(str(int(self.convert_int_to_bin(struct.unpack_from('HHHH',datagram,offset)[1],16)[8:16],2)))
                adresseIP=movieIP[0]+'.'+movieIP[1]+'.'+movieIP[2]+'.'+movieIP[3]
                port=struct.unpack_from('HHHH',datagram,offset)[2]
                offset+=8+len_title
                self.movielist.append((title,adresseIP,port))
            self.etatClient='ready'
            self.clientProxy.initCompleteONE(self.userlist,self.movielist)                    
            
        if typep == '1111' :
            if self.etatClient=='waitJoinMovieAck':
                print 'ok'
                if ack_num==self.ack_cmp:
                    print 'ok 1'
                    self.clientProxy.joinRoomOKONE()
                    self.incrementer_ackcompteur()
                    
            if self.etatClient=='waitJoinMainAck':
                
                if ack_num==self.ack_cmp:
                    print 'ok 2'
                    self.clientProxy.joinRoomOKONE()
                    self.incrementer_ackcompteur()
