# -*- coding: utf-8 -*-
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from c2w.main.lossy_transport import LossyTransport
import logging
import ctypes
import struct
from c2w.main.constants import ROOM_IDS
logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.udp_chat_client_protocol')


class c2wUdpChatClientProtocol(DatagramProtocol):

    def __init__(self, serverAddress, serverPort, clientProxy, lossPr):
        """
        :param serverAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param serverPort: The port number used by the c2w server,
            given by the user.
        :param clientProxy: The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attributes:

        .. attribute:: serverAddress

            The IP address (or the name) of the c2w server.

        .. attribute:: serverPort

            The port number used by the c2w server.

        .. attribute:: clientProxy

            The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        .. attribute:: lossPr

            The packet loss probability for outgoing packets.  Do
            not modify this value!  (It is used by startProtocol.)

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """

        self.serverAddress = serverAddress
        self.serverPort = serverPort
        self.clientProxy = clientProxy
        self.lossPr = lossPr
        self.ack_cmp=0 #numéro du dernier paquet reçu du serveur
        self.seq_cmp=0 #compteur du numéro de séquence envoyé
        self.user_id=0
        self.salleactuelle='main' #type de salle où l'utilisateur se trouve en ce moment
        self.movie_id=''
        self.userlist=[]
        self.main_userlist=[]
        self.movielist=[]
        self.etatClient='' #cette variable indique si le client attend un acquittement ou pas.
        self.username_id=[]
        self.call=''
        self.lastSent=''
    def startProtocol(self):
        """
        DO NOT MODIFY THE FIRST TWO LINES OF THIS METHOD!!

        If in doubt, do not add anything to this method.  Just ignore it.
        It is used to randomly drop outgoing packets if the -l
        command line option is used.
        """
        self.transport = LossyTransport(self.transport, self.lossPr)
        DatagramProtocol.transport = self.transport

    def sendLoginRequestOIE(self, userName):
        """
        :param string userName: The user name that the user has typed.

        The client proxy calls this function when the user clicks on
        the login button.
        """
        self.type="0001"
        self.ack_flag="0"
        self.reserved="0"
        self.ack_num=0 #ackflag est a 0=>cette valeur importe peu
        self.seq_num=self.seq_cmp
        self.data_len=len(userName)
        self.header=self.create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) 
        self.msg_len=self.create_msg_len(self.data_len) 
        self.buffer=self.prepare_buffer_login(self.header,self.msg_len,userName,self.data_len) 
        self.send_packet(self.buffer,self.serverAddress,self.serverPort) 
        moduleLogger.debug('loginRequest called with username=%s', userName)
        self.etatClient='waitAck'
        self.incrementer_seqcompteur()
        print "login request sentwith seq_num", self.seq_cmp-1

    def sendChatMessageOIE(self, message):
        """
        :param message: The text of the chat message.
        :type message: string

        Called by the client proxy  when the user has decided to send
        a chat message

        .. note::
           This is the only function handling chat messages, irrespective
           of the room where the user is.  Therefore it is up to the
           c2wChatClientProctocol or to the server to make sure that this
           message is handled properly, i.e., it is shown only by the
           client(s) who are in the same room.
        """
        self.type="0100"
        self.ack_flag="0"
        self.reserved="0"
        self.ack_num=self.ack_cmp
        self.seq_num=self.seq_cmp
        
        self.incrementer_seqcompteur()
        
        print "salleactuelle",self.salleactuelle[0:4] 
        
        if self.salleactuelle=='main':
            movie_id="0"*48
            self.data_len=8+len(message)
            self.header=self.create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) 
            self.msg_len=self.create_msg_len(self.data_len) 
        
        
            
        elif self.salleactuelle[0:5]=='movie':
            self.data_len=8+len(message)
            self.header=self.create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) 
            self.msg_len=self.create_msg_len(self.data_len)
            for movie in self.movielist:
                if movie[0]==self.salleactuelle[6:]:
                    movieIP=movie[1]
                    moviePort=movie[2]
            movieIP=movieIP.split('.',3)
            for i in range(0,4):
                movieIP[i]=self.convert_int_to_bin(int(movieIP[i]),8)
            moviePort=self.convert_int_to_bin(moviePort,16)
            movie_id=movieIP[0]+movieIP[1]+movieIP[2]+movieIP[3]+moviePort

        self.buffer=self.prepare_buffer_msg(self.header,self.msg_len,self.user_id,self.data_len,movie_id,message) 
        self.send_packet(self.buffer,self.serverAddress,self.serverPort)
        self.etatClient='waitMsgAck'
        
        

    def sendJoinRoomRequestOIE(self, roomName):
        """
        :param roomName: The room name (or movie title.)

        Called by the client proxy  when the user
        has clicked on the watch button or the leave button,
        indicating that she/he wants to change room.

        .. warning:
            The controller sets roomName to
            c2w.main.constants.ROOM_IDS.MAIN_ROOM when the user
            wants to go back to the main room.
        """
        if roomName==ROOM_IDS.MAIN_ROOM:
            self.type="0111"
            self.ack_flag="0"
            self.reserved="0"
            self.ack_num=0
            self.seq_num=self.seq_cmp
            self.incrementer_seqcompteur()
            self.data_len=2
            self.header=self.create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num)
            self.msg_len=self.create_msg_len(self.data_len) 
            self.buffer=self.prepare_buffer_joinroom(self.header,self.msg_len,self.user_id,self.data_len) 
            self.send_packet(self.buffer,self.serverAddress,self.serverPort) 
            #self.etatClient='waitJoinMainAck'
            self.salleactuelle='main'
            self.clientProxy.joinRoomOKONE()
            print"join main room request sent avec seq_num",self.seq_cmp-1
            print "\n"
            
        else:
            self.type="0110"
            self.ack_flag="0"
            self.reserved="0"
            self.ack_num=0
            self.seq_num=self.seq_cmp
            self.incrementer_seqcompteur()
            self.data_len=8
            self.header=self.create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num)
            self.msg_len=self.create_msg_len(self.data_len)
            for movie in self.movielist:
                if movie[0]==roomName:
                    movieIP=movie[1]
                    moviePort=movie[2]
            self.clientProxy.updateMovieAddressPort(roomName, movieIP, moviePort)
            movieIP=movieIP.split('.',3)
            for i in range(0,4):
                movieIP[i]=self.convert_int_to_bin(int(movieIP[i]),8)
            moviePort=self.convert_int_to_bin(moviePort,16)
            movie_id=movieIP[0]+movieIP[1]+movieIP[2]+movieIP[3]+moviePort
            self.buffer=self.prepare_buffer_joinroommov(self.header,self.msg_len,self.user_id,movie_id,self.data_len)
            self.send_packet(self.buffer,self.serverAddress,self.serverPort)
            #self.etatClient='waitJoinMovieAck'
            self.clientProxy.joinRoomOKONE()
            self.salleactuelle='movie:'+roomName
            print "join movie room request sent avec seq_num", self.seq_cmp-1
            print "\n"
            
    def sendLeaveSystemRequestOIE(self):
        """
        Called by the client proxy  when the user
        has clicked on the leave button in the main room.
        """
        self.type="1000"
        self.ack_flag="0"
        self.reserved="0"
        self.ack_num=0
        self.seq_num=self.seq_cmp
        self.incrementer_seqcompteur()
        self.data_len=2
        self.header=self.create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num)
        self.msg_len=self.create_msg_len(self.data_len) 
        self.buffer=self.prepare_buffer_joinroom(self.header,self.msg_len,self.user_id,self.data_len) 
        self.send_packet(self.buffer,self.serverAddress,self.serverPort) 
        print"leave request sent avec seq_num",self.seq_cmp-1
        print "\n"
        self.etatClient='waitAck'
        self.clientProxy.leaveSystemOKONE()

        
    def acknowledgement(self):
        self.type="1111"
        self.ack_flag="1"
        self.reserved="0"
        self.ack_num=self.ack_cmp
        self.seq_num=0
        self.data_len=0
        self.header=self.create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) 
        self.msg_len=self.create_msg_len(self.data_len) 
        self.buffer=self.prepare_buffer_ack(self.header,self.msg_len) 
        self.send_packet_ack(self.buffer,self.serverAddress,self.serverPort)
        print "le client a envoyé un ack pour le msg du server num",self.ack_num
    
    def acknowledgement_p(self):
        self.type="1111"
        self.ack_flag="1"
        self.reserved="0"
        if self.ack_cmp!=0:
            self.ack_num=self.ack_cmp
        else:
            self.ack_num=0
        self.seq_num=0
        self.data_len=0
        self.header=self.create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) 
        self.msg_len=self.create_msg_len(self.data_len) 
        self.buffer=self.prepare_buffer_ack(self.header,self.msg_len) 
        self.send_packet_ack(self.buffer,self.serverAddress,self.serverPort)
        print "le client a renvoyé un ack pour le msg du server num",self.ack_num
    
    def create_header(self,typep,ackflag,reserved,ack_num,seq_num):
        # On convertit en binaire le paramètre ack_num et on prend la sous-chaine à partir du deuxième caractère
        # pour supprimer '0b' qu'on trouve devant les chaines binaires        
        ack_num=str(bin(ack_num)[2:])
        # On complète la séquence binaire de ack_num à gauche par des 0 pour faire 13 bits (qui est la taille du champ ack_num)        
        z=str("0"*(13-len(ack_num)))
        ack_num=z+ack_num
        # On convertit en binaire le paramètre seq_num et on prend la sous-chaine à partir du deuxième caractère
        # pour supprimer '0b' qu'on trouve devant les chaines binaires 
        seq_num=str(bin(seq_num)[2:]) 
        # On complète la séquence binaire de seq_num à gauche par des 0 pour faire 13 bits (qui est la taille du champ seq_num) 
        z=str("0"*(13-len(seq_num))) 
        seq_num=z+seq_num
        # On convertit en décimal l'entête
        header=int(typep+ackflag+reserved+ack_num+seq_num,2)
        return header
        
    def create_msg_len(self,data_len):
        # La taille paquet correspond à la taille des données + la taille de l'entête (6 octets)
        msg_len=data_len+6
        # On convertit en binaire la taille du paquet et on prend la sous-chaine à partir du deuxième caractère
        # pour supprimer '0b' qu'on trouve devant les chaines binaires        
        msg_len=str(bin(msg_len)[2:])
        # On complète la séquence binaire de msg_len à gauche par des 0 pour faire 16 bits (qui est la taille du champ length du paquet)        
        z=str("0"*(16-len(msg_len)))
        msg_len=z+msg_len
        # On convertit en décimal la taille du paquet ce qui nous donne la valeur du champ length        
        msg_len=int(msg_len,2)
        return msg_len
        
    def prepare_buffer_login(self,header,msg_len,data,data_len):
        # On construit un buffer de taille la taille du paquet (6 octets pour l'entête + la taille des données)        
        buff=ctypes.create_string_buffer(6+data_len)
        # On met les données dans le buffer
        struct.pack_into('>IH'+str(data_len)+'s',buff,0,header,msg_len,data)
        return buff
       
    def prepare_buffer_joinroom(self,header,msg_len,data,data_len):
        buff=ctypes.create_string_buffer(6+data_len)
        struct.pack_into('>IHH',buff,0,header,msg_len,data)
        return buff
    
    def prepare_buffer_joinroommov(self,header,msg_len,userid,movieid,data_len):
        buff=ctypes.create_string_buffer(14)
        struct.pack_into('>IHHHHH',buff,0,header,msg_len,userid,int(movieid[0:16],2),int(movieid[16:32],2),int(movieid[32:48],2))
        return buff
    
    def prepare_buffer_msg(self,header,msg_len,user_id,data_len,movie_id,message):
        buff=ctypes.create_string_buffer(6+data_len)
        
        struct.pack_into('>IHHHHH'+str(len(message))+'s',buff,0,header,msg_len,user_id,int(movie_id[0:16],2),int(movie_id[16:32],2),int(movie_id[32:48],2),message)
        return buff
    
    def prepare_buffer_ack(self,header,msg_len):
        buff=ctypes.create_string_buffer(6)
        struct.pack_into('>IH',buff,0,header,msg_len)
        return buff
    
        
    def send_packet(self,buff,serverAddress,serverPort):
        self.transport.write(buff.raw,(serverAddress,serverPort))
        self.lastSent=buff
        if True:
            self.call=reactor.callLater(2,self.send_packet,buff,serverAddress,serverPort)
            
            
    
    def send_packet_ack(self,buff,serverAddress,serverPort):
        self.transport.write(buff.raw,(serverAddress,serverPort))
            
    def incrementer_seqcompteur(self):
        self.seq_cmp = (self.seq_cmp+1)%8192  

            
    def incrementer_ackcompteur(self):
        self.ack_cmp = (self.ack_cmp+1)%8192

    def datagramReceived(self, datagram, (host, port)):
        """
        :param string datagram: the payload of the UDP packet.
        :param host: the IP address of the source.
        :param port: the source port.

        Called **by Twisted** when the client has received a UDP
        packet.
        """
        
        header=self.unpack_header(datagram)
        typep=header[0:4]
        ackflag=header[4]
        reserved=header[5]
        ack_num=int(header[6:19],2)
        seq_num=int(header[19:32],2)
        msg_len=self.unpack_msg_len(datagram)
        msg_len=int(msg_len,2)
        
        print "nouveau message reçu"

        if ackflag=='1':

            print "ack attendu",self.seq_cmp-1
            print "trame acquitée num",ack_num
        else :
            print "num de seq envoyée par le serveur",seq_num
        print "ack_cmp++++",self.ack_cmp
        print "seq_num+++",seq_num
        
        if seq_num==self.ack_cmp and seq_num!=0:
            self.acknowledgement_p()
            
        if seq_num==self.ack_cmp+1 or (seq_num==0 and (typep=='1110' or typep=='0000')):
            if seq_num==self.ack_cmp+1:
                self.incrementer_ackcompteur()
            if typep == "0010" and self.userlist==[]:#le client reçoit la liste des utilisateurs
                    self.acknowledgement()
                    number=struct.unpack_from('>IHH',datagram,0)[2]
                    #self.etatClient='waitMovieList'
                    offset=8
                    for i in range(0,number):
                        len_username=bin(struct.unpack_from('>BH',datagram,offset)[0])
                        s=len_username[len(len_username)-1]
                        len_username=int(len_username[2:len(len_username)-1],2)
                        username=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[2]
                        user_id=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[1]
                        self.username_id.append((username,user_id))
                        offset+=3+len_username
                        if s=='1':
                            self.userlist.append((username,ROOM_IDS.MAIN_ROOM))
                        else:
                            self.userlist.append((username,ROOM_IDS.MOVIE_ROOM))
                    print "user list received \n"
                    
            elif typep == "0010" and self.userlist != []:#le client reçoit une mise à jour de liste des utilisateurs
                    self.acknowledgement()
                    self.userlist=[]
                    number=struct.unpack_from('>IHH',datagram,0)[2]
                    #self.etatClient='waitMovieList'
                    offset=8
                    if self.salleactuelle[0:5]=="movie":
                        self.userlist=[]
                        for i in range(0,number):
                            len_username=bin(struct.unpack_from('>BH',datagram,offset)[0])
                            s=len_username[len(len_username)-1]
                            len_username=int(len_username[2:len(len_username)-1],2)
                            username=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[2]
                            print "username movie",username
                            user_id=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[1]
                            self.username_id.append((username,user_id))
                            offset+=3+len_username
                            self.userlist.append((username,self.salleactuelle[6:]))
                        self.clientProxy.setUserListONE(self.userlist)
                        print 'up0',self.userlist
                    elif self.salleactuelle=='main':
                        for i in range(0,number):
                            len_username=bin(struct.unpack_from('>BH',datagram,offset)[0])
                            s=len_username[len(len_username)-1]
                            len_username=int(len_username[2:len(len_username)-1],2)
                            username=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[2]
                            user_id=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[1]
                            self.username_id.append((username,user_id))
                            offset+=3+len_username
                            if s=='1':
                                self.userlist.append((username,ROOM_IDS.MAIN_ROOM))
                            else:
                                self.userlist.append((username,ROOM_IDS.MOVIE_ROOM))
                        self.clientProxy.setUserListONE(self.userlist)
                
            
            
                #self.acknowledgement()
                #number=struct.unpack_from('>IHH',datagram,0)[2]
                #offset=8
                #print "salle ", self.salleactuelle
                #if self.salleactuelle[0:5]=="movie":
                #    self.userlist=[]
                #    for i in range(0,number):
                #        len_username=bin(struct.unpack_from('>BH',datagram,offset)[0])
                #        s=len_username[len(len_username)-1]
                #        len_username=int(len_username[2:len(len_username)-1],2)
                #        username=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[2]
                #        print "username movie",username
               #         user_id=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[1]
                #        self.username_id.append((username,user_id))
                #        offset+=3+len_username
                #        self.userlist.append((username,self.salleactuelle[6:]))
                #    self.clientProxy.setUserListONE(self.userlist)
                #    print 'up0',self.userlist
                #elif self.salleactuelle=='main':
                 #   
                #    if number <= len(self.main_userlist):
                #        exists=False
                #        for i in range(0,number):
                #            len_username=self.convert_int_to_bin(struct.unpack_from('>BH',datagram,offset)[0],8)
                 #           s=len_username[len(len_username)-1]
                #            if s=='0':
                #                room=ROOM_IDS.MOVIE_ROOM
                #            elif s=='1':
                 #               room=ROOM_IDS.MAIN_ROOM
                #            len_username=int(len_username[:len(len_username)-1],2)
                #            username=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[2]
                #            print "userame",username
                #            print "list0",self.userlist
                #            user_id=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[1]
                #            offset+=3+len_username
#
                 #           t=0
                            
                #            for k in range(0,len(self.userlist)):                      
                #                print "list1",self.userlist
                #                if username==self.userlist[k][0]:
                #                    exists=True
                #                    t=k
                #            print "list2",self.userlist
                #            if exists==True:
                #                print "room1",room
                #                print "room2",self.userlist[t][1]
                #                if room!=self.userlist[t][1]:
                #                    print "list3",self.userlist
                #                    self.clientProxy.userUpdateReceivedONE(username,room)
                #                    del self.userlist[t]
                #                    self.userlist.append((username,room))
                #                    print "upd2",self.userlist
                #                    
                #            elif exists==False:
                #                print "out"
                #                self.clientProxy.userUpdateReceivedONE(username,ROOM_IDS.OUT_OF_THE_SYSTEM_ROOM)
                #                self.userlist.remove((self.userlist[t][0],self.userlist[t][1]))
                #                for user1 in self.username_id:
                #                    if self.userlist[t][0] == user1[0]:
                #                        self.username_id.remove((user1[0],user1[1])) 
                            
                        
                             
                #    elif number > len(self.main_userlist):
                #        print "len"
                #        exists=False
                #        for i in range(0,number):
                #            len_username=self.convert_int_to_bin(struct.unpack_from('>BH',datagram,offset)[0],8)
                #            print "len_username",len_username
                #            s=len_username[len(len_username)-1]
                #            if s=='0':
                #                room=ROOM_IDS.MOVIE_ROOM
                #            elif s=='1':
                #                room=ROOM_IDS.MAIN_ROOM
                #            len_username=int(len_username[:len(len_username)-1],2)
                #            username=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[2]
                #            print "userame",username
                #            user_id=struct.unpack_from('>BH'+str(len_username)+'s',datagram,offset)[1]
                #            offset+=3+len_username
#
                #            t=0
                #            
                #            for k in range(0,len(self.userlist)):                      
                #                
                #                if username==self.userlist[k][0]:
                #                    exists=True
                #                    t=k
                #             
                #            
                #            if exists==False: 
                #                self.clientProxy.userUpdateReceivedONE(username,room)
                #                self.userlist.append((username,room))
                #                self.username_id.append((username,user_id))
                #                print "upd1",self.userlist
                #    
                #    self.main_userlist=self.userlist
                #print "update user list received \n"
            if typep == "0011": #and self.etatClient=='waitMovieList':#le client reçoit la liste des films
                    self.acknowledgement()
                    number=struct.unpack_from('>IHH',datagram,0)[2]
                    offset=8
                    for i in range(0,number):
                        len_title=bin(struct.unpack_from('>HHHB',datagram,offset)[3])
                        len_title=int(len_title,2)
                        title=struct.unpack_from('>HHHB'+str(len_title)+'s',datagram,offset)[4]
                        movieIP=[]
                        movieIP.append(str(int(self.convert_int_to_bin(struct.unpack_from('>HHHB',datagram,offset)[0],16)[0:8],2)))
                        movieIP.append(str(int(self.convert_int_to_bin(struct.unpack_from('>HHHB',datagram,offset)[0],16)[8:16],2)))
                        movieIP.append(str(int(self.convert_int_to_bin(struct.unpack_from('>HHHB',datagram,offset)[1],16)[0:8],2)))
                        movieIP.append(str(int(self.convert_int_to_bin(struct.unpack_from('>HHHB',datagram,offset)[1],16)[8:16],2)))
                        adresseIP=movieIP[0]+'.'+movieIP[1]+'.'+movieIP[2]+'.'+movieIP[3]
                        port=struct.unpack_from('>HHHB',datagram,offset)[2]
                        offset+=7+len_title
                        self.movielist.append((title,adresseIP,port))
                    self.etatClient='ready'
                    self.clientProxy.initCompleteONE(self.userlist,self.movielist)     
                                
                    print "movie list received\n"
                    
                
            if typep == "0100": #le client reçoit un message à afficher
                user_id=struct.unpack_from('>IHHHHH',datagram,0)[2]
                msg=struct.unpack_from('>IHHHHH'+str(msg_len-14)+'s',datagram,0)[6]
                for user in self.username_id:
                    if user[1]==user_id:
                        username=user[0]
                self.clientProxy.chatMessageReceivedONE(username,msg) 
                self.acknowledgement()
                
            if  ackflag=='1' and self.seq_cmp-1==ack_num:
                # ajouter if ack_num == seq.cmp
                if typep == "1110" and self.etatClient=='waitAck': #le client reçoit response_login_ok
                    print " Id received \n"    
                    if self.call!='':
                        self.call.cancel() 
                        print 'cancel'
                        self.call=''
                    self.user_id=struct.unpack_from('>IHH',datagram,0)[2]
                    #self.etatClient='waitUserList'
                    self.acknowledgement()
                     
                              
                    
            if typep == "0000": #and self.etatClient=='waitLoginAck': #le client reçoit msg d'erreur
                print "++++++"
                data=struct.unpack_from('>IHH',datagram,0)[2]
                self.etatClient=''
                if data==1:
                    if self.call!='':        
                        self.call.cancel() 
                        print 'cancel'
                        self.call=''
                    self.clientProxy.connectionRejectedONE("Invalid User Name/Connection Denied")
                    self.acknowledgement()
                    
                
        
        
        
        if typep == '1111' and self.seq_cmp-1==ack_num:
                    if ack_num==self.seq_cmp-1:
                        if self.call!='':        
                            self.call.cancel() 
                            print 'cancel'
                            self.call=''
    
        
            
        
    def unpack_header(self,datagram): #fonction qui permet d'avoir une chaine de car de 32 bits contenant le header
        header = struct.unpack_from('>IH',datagram)[0]
        header=bin(header)
        header=str(header)[2:]
        z="0"*(32-len(header))
        header=z+header
        return header
        
    def unpack_msg_len(self,datagram): #fonction qui permet d'avoir une chaine de car de 16 bits contenant le msg_len
        msg_len = struct.unpack_from('>IH',datagram)[1]
        msg_len=bin(msg_len)
        msg_len=str(msg_len)[2:]
        z="0"*(16-len(msg_len))
        msg_len=z+msg_len
        return msg_len
        
 
        
    def convert_int_to_bin(self,integ,taille): #fonction qui permet de convertir un entier en une chaine de caractere de la representation binaire de cet entier avec une taille predefinie .
        sortie=str(bin(integ)[2:])
        z=str("0"*(taille-len(sortie)))
        sortie=z+sortie
        return sortie
