# -*- coding: utf-8 -*-
from twisted.internet.protocol import Protocol
import logging
import ctypes
import struct
from c2w.main.constants import ROOM_IDS
from utile import *

logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.tcp_chat_client_protocol')

class c2wTcpChatClientProtocol(Protocol):

    def __init__(self, clientProxy, serverAddress, serverPort):
        """
        :param clientProxy: The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.
        :param serverAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param serverPort: The port number used by the c2w server,
            given by the user.

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: clientProxy

            The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        .. attribute:: serverAddress

            The IP address (or the name) of the c2w server.

        .. attribute:: serverPort

            The port number used by the c2w server.

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """
        self.serverAddress = serverAddress
        self.serverPort = serverPort
        self.clientProxy = clientProxy


        self.ack_cmp=0 #compteur du numéro d'acquittement
        self.seq_serveur = 0
        self.seq_cmp=0 #compteur du numéro de séquence
        self.user_id=0
        self.salleactuelle='' #type de salle où l'utilisateur se trouve en ce moment
        self.movie_id=''
        self.userlist=[]
        self.movielist=[]
        self.etatClient='' #cette variable indique si le client attend un acquittement ou pas.
        self.username_id=[]

        self.buff = ''
        self.taille = 99999999

    def sendLoginRequestOIE(self, userName):
        """
        :param string userName: The user name that the user has typed.

        The client proxy calls this function when the user clicks on
        the login button.
        """
        moduleLogger.debug('loginRequest called with username=%s', userName)
        username = userName

        self.prepare_header("0001", "0", len(username))
        
        if self.etatClient == 'ready':
            self.ack_flag = "1"

        # self.incrementer_seqcompteur()

        self.header = create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) #Fonction qu'on a ajouté voir fin du fichier
        self.msg_len = create_msg_len(self.data_len) #Fonction qu'on a ajouté voir fin du fichier
        self.buffer = self.prepare_buffer_login(self.header,self.msg_len,userName,self.data_len) #Fonction qu'on a ajouté voir fin du fichier

        print 'send_packet'
        self.send_packet(self.buffer) #Fonction qu'on a ajouté voir fin du fichier
        self.etatClient='waitLoginAck'

    def sendChatMessageOIE(self, message):
        """
        :param message: The text of the chat message.
        :type message: string

        Called by the client proxy when the user has decided to send
        a chat message

        .. note::
           This is the only function handling chat messages, irrespective
           of the room where the user is.  Therefore it is up to the
           c2wChatClientProctocol or to the server to make sure that this
           message is handled properly, i.e., it is shown only by the
           client(s) who are in the same room.
        """

        self.prepare_header("0100", "0", 8 + len(message)) 
        
        self.header = create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num) 
        self.msg_len = create_msg_len(self.data_len) 
        # self.incrementer_seqcompteur()
        movie_id="0"*48
        if self.salleactuelle=='main':
            pass            
            
        elif self.salleactuelle[0:5]=='movie':
            for movie in self.movielist:
                if movie[0]==self.salleactuelle[6:]:
                    movieIP=movie[1]
                    moviePort=movie[2]
            movieIP=movieIP.split('.',3)
            for i in range(0,4):
                movieIP[i] = convert_int_to_bin(int(movieIP[i]),8)
            moviePort = convert_int_to_bin(moviePort,16)
            movie_id=movieIP[0]+movieIP[1]+movieIP[2]+movieIP[3]+moviePort

        self.buffer=self.prepare_buffer_msg(movie_id,message) 
        self.send_packet(self.buffer)
        
        self.etatClient='waitMsgAck'

    def sendJoinRoomRequestOIE(self, roomName):
        """
        :param roomName: The room name (or movie title.)

        Called by the client proxy  when the user
        has clicked on the watch button or the leave button,
        indicating that she/he wants to change room.

        .. warning:
            The controller sets roomName to
            c2w.main.constants.ROOM_IDS.MAIN_ROOM when the user
            wants to go back to the main room.
        """
        if roomName==ROOM_IDS.MAIN_ROOM:

            self.prepare_header("0111", "0", 2)
            # self.incrementer_seqcompteur()
            self.header = create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num)
            self.msg_len = create_msg_len(self.data_len) 

            self.buffer=self.prepare_buffer_joinroom(self.header,self.msg_len,self.user_id,self.data_len) 
            self.send_packet(self.buffer,self.serverAddress,self.serverPort) 
            self.etatClient='waitJoinMainAck'
            self.salleactuelle='main'
            
            
        else:
            self.prepare_header("0110", "0", 8)
            # self.incrementer_seqcompteur()
            self.header = create_header(self.type,self.ack_flag,self.reserved,self.ack_num,self.seq_num)
            self.msg_len = create_msg_len(self.data_len) 

            for movie in self.movielist:
                if movie[0]==roomName:
                    movieIP=movie[1]
                    moviePort=movie[2]
            self.clientProxy.updateMovieAddressPort(roomName, movieIP, moviePort)
            movieIP=movieIP.split('.',3)
            for i in range(0,4):
                movieIP[i] = convert_int_to_bin(int(movieIP[i]),8)
            moviePort = convert_int_to_bin(moviePort,16)
            movie_id=movieIP[0]+movieIP[1]+movieIP[2]+movieIP[3]+moviePort

            self.buffer=self.prepare_buffer_joinroommov(self.header,self.msg_len,self.user_id,movie_id,self.data_len)
            self.send_packet(self.buffer,self.serverAddress,self.serverPort)
            self.etatClient='waitJoinMovieAck'
            self.salleactuelle='movie:'+roomName

    def sendLeaveSystemRequestOIE(self):
        """
        Called by the client proxy  when the user
        has clicked on the leave button in the main room.
        """

        self.prepare_header("1000", "0", 2)

        self.header = create_header(self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.msg_len = create_msg_len(self.data_len)

        self.buffer = self.prepare_buffer_logout(self.header, self.msg_len, self.user_id, self.data_len)
        self.send_packet(self.buffer, self.serverAddress, self.serverPort)

        self.etatClient = 'waitLeaveAck'


################################################################################
    def prepare_header(self, typep, ackflag, length):
        self.type = typep
        self.ack_flag=ackflag
        self.reserved="0"
        self.ack_num=self.ack_cmp
        self.seq_num=self.seq_cmp
        self.data_len = length

    def prepare_buffer_login(self,header,msg_len,data,data_len):
        buff=ctypes.create_string_buffer(6+data_len)
        struct.pack_into('>IH'+str(data_len)+'s',buff,0,header,msg_len,data)
        return buff
       
    def prepare_buffer_joinroom(self,header,msg_len,data,data_len):
        buff=ctypes.create_string_buffer(6+data_len)
        struct.pack_into('>IHH',buff,0,header,msg_len,data)
        return buff
    
    def prepare_buffer_joinroommov(self,header,msg_len,userid,movieid,data_len):
        buff=ctypes.create_string_buffer(14)
        struct.pack_into('>IHHHHH',buff,0,header,msg_len,userid,int(movieid[0:16],2),int(movieid[16:32],2),int(movieid[32:48],2))
        return buff
    
    def prepare_buffer_msg(self,movie_id,message):
        buff=ctypes.create_string_buffer(6+self.data_len)
        struct.pack_into('>IHHHHH'+str(len(message))+'s',buff,0,self.header,self.msg_len,self.user_id,int(movie_id[0:16],2),int(movie_id[16:32],2),int(movie_id[32:48],2),message)
        return buff
    
    def prepare_buffer_ack(self,header,msg_len):
        buff=ctypes.create_string_buffer(6)
        struct.pack_into('>IH',buff,0,header,msg_len)
        return buff
    
        
    def send_packet(self,buff):
        self.transport.write(buff.raw)
        self.seq_cmp += 1 % 8192
        self.seq_serveur += 1 % 8912
    
    def acknowledgement(self):
        self.prepare_header("1111", "1", 0)
        self.seq_num = 0
        self.header = create_header(self.type, self.ack_flag, self.reserved, self.ack_num, self.seq_num)
        self.msg_len = create_msg_len(self.data_len)
        
        self.buffer=self.prepare_buffer_ack(self.header,self.msg_len) 
        self.send_packet(self.buffer)
        print "le client a envoyé un ack pour le msg du server num",self.ack_num

    def dataReceived(self, data):
        """
        :param data: The message received from the server
        :type data: A string of indeterminate length

        Twisted calls this method whenever new data is received on this
        connection.
        """
        self.buff=self.buff+data
        print "---->data received" 
        while(len(self.buff)>=6):
            print 'len_buff:',len(self.buff)
            if(len(self.buff)>=6 and self.taille == 99999999):        
                self.taille=struct.unpack_from('>IH',self.buff)[1]
                print 'taille',self.taille
            if len(self.buff) >= self.taille:
                header = unpack_header(self.buff)
                typep=header[0:4]
                print 'type:',typep
                ackflag=header[4]
                reserved=header[5]
                ack_num=int(header[6:19],2)
                seq_num=int(header[19:32],2)
                print 'receive_seq_num:',seq_num
                msg_len=unpack_msg_len(self.buff)
                msg_len=int("0b"+msg_len,2)                

                ##############################################
                print "seqnum",self.seq_cmp
                print "acknum",self.ack_cmp
                
                print "type=",typep
                if typep != '1111':
                    self.ack_cmp=seq_num

                #le client reçoit response_login_ok
                if typep == "1110" and self.etatClient=='waitLoginAck': 
                    print "--->reçoit login ok"
                    print "ack:",ack_num
                    print "seq_num:",seq_num
                    print "seq_cmp:",self.seq_cmp
                    if ackflag == '1' and ack_num == self.seq_cmp - 1:
                        self.user_id=struct.unpack_from('>IHH',self.buff,0)[2]
                        print '!!!id reserved'
                        self.etatClient='waitUserList'
                        self.acknowledgement()


                

                #le client reçoit msg d'erreur    
                if typep=="0000" and self.etatClient=='waitLoginAck': 

                    print "--->reçoit msg d'erreur"
                    print "ack:",ack_num
                    print "seq_num:",seq_num
                    print "seq_cmp:",self.seq_cmp
                    if ackflag == '1' and ack_num == self.seq_cmp - 1:
                        data=struct.unpack_from('>IHB',self.buff,0)[2]
                        self.etatClient='ready'
                        print "error=",data
                        if data == 1:
                            self.clientProxy.connectionRejectedONE("Invalid User Name/Connection Denied")
                        if data == 2:
                            self.clientProxy.connectionRejectedONE("Unknown User")
                        if data == 3:
                            self.clientProxy.connectionRejectedONE("Unknown Movie")
                        if data == 4:
                            self.clientProxy.connectionRejectedONE("Malformed Message")

                #le client reçoit la liste des utilisateurs
                if typep == "0010":
                    print "--->reçoit la liste des utilisateurs"
                    print "ack:",ack_num
                    print "seq_num:",seq_num
                    print "seq_cmp:",self.seq_cmp
                    #if ackflag == '1' and ack_num == self.seq_cmp - 1:
                    number=struct.unpack_from('>IHH',self.buff,0)[2]
                    if self.etatClient == 'waitUserList':
                        self.etatClient='waitMovieList'
                    offset=8
                    userlist = []     
                    username_id = []
                    for i in range(0,number):
                        len_username=bin(struct.unpack_from('>BH',self.buff,offset)[0])
                        s=len_username[len(len_username)-1]
                        len_username=int(len_username[2:len(len_username)-1],2)
                        user_id = struct.unpack_from('>BH'+str(len_username)+'s',self.buff,offset)[1]
                        username=struct.unpack_from('>BH'+str(len_username)+'s',self.buff,offset)[2]
                        offset+=3+len_username
                        username_id.append((username,user_id))
                        if s=='1':
                            userlist.append((username,ROOM_IDS.MAIN_ROOM))
                        else:
                            userlist.append((username,ROOM_IDS.MOVIE_ROOM))
                    self.userlist = userlist
                    self.username_id = username_id
                    if self.salleactuelle[0:5] == 'movie':
                        for j in range(0,number):
                            userlist[j][1] = self.salleactuelle[6:]
                        self.clientProxy.setUserListONE(self.userlist)
                        print "update for movieroom:",self.salleactuelle[6:]
                    elif self.salleactuelle == 'main':
                        self.clientProxy.setUserListONE(self.userlist)
                        print "update for mainroom"

                    self.acknowledgement()
                # ajoute RO dans cet fonction? meme si c'est pas la premiere fois.
                # mise a jour n'est pas fini............................
                #########################################

                #le client rocoit la liste des movies
                if typep == "0011" and self.etatClient=='waitMovieList':#le client reçoit la liste des films
                    print "--->reçoit la liste des movies"
                    print "ack:",ack_num
                    print "seq_num:",seq_num
                    print "seq_cmp:",self.seq_cmp
                    number=struct.unpack_from('>IHH',self.buff,0)[2]
                    offset=8
                    for i in range(0,number):
                        len_title=bin(struct.unpack_from('>HHHB',self.buff,offset)[3])
                        len_title=int(len_title,2)
                        title=struct.unpack_from('>HHHB'+str(len_title)+'s',self.buff,offset)[4]
                        movieIP=[]
                        movieIP.append(str(int(convert_int_to_bin(struct.unpack_from('>HHHB',self.buff,offset)[0],16)[0:8],2)))
                        movieIP.append(str(int(convert_int_to_bin(struct.unpack_from('>HHHB',self.buff,offset)[0],16)[8:16],2)))
                        movieIP.append(str(int(convert_int_to_bin(struct.unpack_from('>HHHB',self.buff,offset)[1],16)[0:8],2)))
                        movieIP.append(str(int(convert_int_to_bin(struct.unpack_from('>HHHB',self.buff,offset)[1],16)[8:16],2)))
                        adresseIP=movieIP[0]+'.'+movieIP[1]+'.'+movieIP[2]+'.'+movieIP[3]
                        port=struct.unpack_from('>HHHB',self.buff,offset)[2]
                        offset+=7+len_title
                        self.movielist.append((title,adresseIP,port))
                    self.etatClient='ready'
                    self.clientProxy.initCompleteONE(self.userlist,self.movielist)                    
                    self.acknowledgement()
                    self.etatClient="mainRoom"
                    self.salleactuelle="main"
                    print 'movie list received'
                    
                # ne pas fini
                if typep == '1111' :
                    print "--->reçoit ack"
                    print "ack:",ack_num
                    print "seq_num:",seq_num
                    print "seq_cmp:",self.seq_cmp
                    if self.etatClient=='waitJoinMovieAck':
                        print 'ok'
                        if ack_num==self.ack_cmp:
                            print 'ok 1'
                            self.clientProxy.joinRoomOKONE()
                            
                    if self.etatClient=='waitJoinMainAck':
                        
                        if ack_num==self.ack_cmp:
                            print 'ok 2'
                            self.clientProxy.joinRoomOKONE()

                #le client recoit un message a afficher
                if typep == "0100":
                    sourceUserId=struct.unpack_from('>IHHHHH',self.buff,0)[2]
                    msg=struct.unpack_from('>IHHHHH'+str(msg_len-14)+'s',self.buff,0)[6]
                    for user in self.username_id:
                        if user[1]==sourceUserId:
                            username=user[0]
                    self.clientProxy.chatMessageReceivedONE(username,msg) 


            #tcp control                
                self.buff=self.buff[self.taille:]
                self.taille=99999999
            else:
                break
