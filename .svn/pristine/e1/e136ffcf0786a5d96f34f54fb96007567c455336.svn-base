import subprocess
import copy
import os.path
import importlib

SVN_DIR = "tmp-SVN"  # would be better to pass as an argument to the script
SVN_REP_SCHEMA = "r302-s14-g"  # spring 2014 dependent
STOCK_RSM_DIR = "tmp-stockrsm"

NO_TEST = [15]

GROUP_SPEC = {
               'g14': [1, 3, 4, 5, 9, 12, 14, 16, 17, 18, 19, 20, 23, 26],
               'g6': [2, 6, 7, 8, 10, 11, 13, 21, 22, 24, 25]
               }

# GROUP_SPEC = {
#                'g14': [12, 14],
#                'g6': [11, 13]
#                }

zero_test_set = set()


def check_group(group_rep_path, myenv):

    scripts_path = os.path.join(group_rep_path, "c2w", "scripts")
    scripts_dir_contents = os.listdir(scripts_path)
    scripts = [e for e in scripts_dir_contents
               if e.startswith("trial_") and e.endswith(".py")]

    passed, movielist_passed = 0, 0
    failed, movielist_failed = 0, 0
    for script in scripts:
        # we need to know the compatibility of the script with respect to spec
        mod = importlib.import_module(script.split('.')[0])
        if myenv["SPEC"] in mod.FOR_SPECS:
            #print script
            p = subprocess.Popen([script], env=myenv,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE, shell=True)
            #print "before p.communicate"
            t_out, _ = p.communicate()
            #print "after p.communicate"

            if "Rock" in t_out:
                if "movielist" in script:
                    movielist_passed += 1
                passed += 1
            else:
                if "movielist" in script:
                    movielist_failed += 1
                failed += 1

    return ((passed, failed), (movielist_passed, movielist_failed))


def main():

    total_passed = 0
    total_failed = 0

    home_path = os.path.expanduser("~")
    stock_rsm_path = os.path.join(home_path, STOCK_RSM_DIR)
    svn_rep_path = os.path.join(home_path, SVN_DIR, SVN_REP_SCHEMA)

    # we should implement a general comparison of sets ...
    assert(set(GROUP_SPEC['g14']).intersection(GROUP_SPEC['g6']) == set([]))
    for spec in list(GROUP_SPEC):
        for g in GROUP_SPEC[spec]:
            my_env = copy.deepcopy(os.environ)
            my_env["PYTHONPATH"] = svn_rep_path + str(g) + ":" + stock_rsm_path
            my_env["PATH"] = (os.path.join(
                                          svn_rep_path + str(g),
                                          "c2w",
                                          "scripts"
                                          )
                              + ":"
                              + my_env["PATH"]
                              )

            my_env["SPEC"] = spec
            res = check_group(svn_rep_path + str(g), my_env)

            tests, movielist_tests = res
            total_passed += tests[0]
            total_failed += tests[1]
            format_str = "{0:>25} : SPEC #{1:<5} {2:<10} ML Tests: {3:<10}"
            if tests[0] == 0:
                zero_test_set.add(g)
            print format_str.format("Group " + str(g),
                                    str(spec),
                                    str(tests[0]) + "/" + str(tests[0] + tests[1]),
                                    str(movielist_tests[0]) + "/" + str(movielist_tests[0] + movielist_tests[1]))

    rate = round(100 * float(total_passed) / float(total_passed + total_failed), 1)
    print "Success Rate :", rate, " %"
    print "zero_test_set :", zero_test_set

if __name__ == '__main__':
    main()
