# -*- coding: utf-8 -*-
from twisted.internet.protocol import DatagramProtocol


class SibylServerUdpBinProtocol(DatagramProtocol):

    def __init__(self, sibylBrain):
        self.sibylBrain = sibylBrain
    def datagramReceived(self, datagram, (host,port)):
        data=self.sibylBrain.generateResponse(datagram)
        self.transport.write(data, (host, port))
