# -*- coding: utf-8 -*-
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
import ctypes
import struct
import math


class SibylServerTimerUdpBinProtocol(DatagramProtocol):

	def __init__(self, sibylBrain):
		self.sibylBrain = sibylBrain
	def datagramReceived(self, datagram, (host,port)):
		print "len=",len(datagram)
		x = math.ceil(math.log(len(datagram)))
		print "x=",x

		data=self.sibylBrain.generateResponse(datagram)
		reactor.callLater(x, self.transport.write(data, (host, port)))
